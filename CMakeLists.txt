﻿cmake_minimum_required (VERSION 3.15)
project (Untitled)

set (CMAKE_CXX_STANDARD 17)
set (CMAKE_CXX_STANDARD_REQUIRED ON)

###############################################################################
# Configure dependencies                                                      #
###############################################################################

# Configure FreeType
set (FT_INSTALL OFF CACHE BOOL "Install FreeType")
add_subdirectory (vendor/freetype-2.12.1)
target_compile_definitions (freetype PUBLIC FT_CONFIG_OPTION_ERROR_STRINGS)

# Configure glad
add_subdirectory (vendor/glad-2.0.2)

# Configure glfw
set (GLFW_BUILD_DOCS OFF CACHE BOOL "Build glfw documentation")
set (GLFW_BUILD_TESTS OFF CACHE BOOL "Build glfw tests")
set (GLFW_BUILD_EXAMPLES OFF CACHE BOOL "Build glfw examples")
set (GLFW_INSTALL OFF CACHE BOOL "Install glfw")
add_subdirectory (vendor/glfw-3.3.8)

# Configure glm
add_subdirectory (vendor/glm-0.9.9.8)

# Configure stb_image
add_subdirectory (vendor/stb_image-2.27)

# Configure stb_image_write
add_subdirectory (vendor/stb_image_write-1.16)

# Configure ImGui
add_subdirectory (vendor/imgui-1.89.1)

###############################################################################
# Configure Untitled                                                          #
###############################################################################

add_subdirectory (source)

# Install assets
install (DIRECTORY assets TYPE BIN FILES_MATCHING REGEX ".*\.png|.*\.vert|.*\.frag")
