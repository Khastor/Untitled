# Untitled

## Dependencies

* [freetype2](https://freetype.org)
* [glad](https://github.com/Dav1dde/glad)
* [glfw](https://github.com/glfw/glfw)
* [glm](https://github.com/g-truc/glm)
* [stb_image](https://github.com/nothings/stb)
* [stb_image_write](https://github.com/nothings/stb)

## Build with CMake

```
$ mkdir build
$ cd build
$ cmake ..
$ cmake --build .
$ cmake --install .
```
