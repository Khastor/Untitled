#include <Core/TerrainObject.hpp>

#include <Core/Coordinates.hpp>
#include <Core/Terrain.hpp>
#include <Core/TerrainFactory.hpp>
#include <Core/TerrainVertexArray.hpp>
#include <Core/Numeric.hpp>

#include <cstdint>

namespace Core
{

TerrainObject::TerrainObject(
    Core::TerrainFactory& pFactory,
    uint32_t pRadius) :
    mRadius(pRadius),
    mPosition{ 0, 0 },
    mFactory(pFactory),
    mVertexArray(pRadius * 2 + 1)
{
    UpdateGraphics();
}

void TerrainObject::CenterOn(
    const Core::WorldCoord& pPosition)
{
    if (mPosition.Xi != pPosition.Xi || mPosition.Yi != pPosition.Yi)
    {
        mPosition.Xi = pPosition.Xi;
        mPosition.Yi = pPosition.Yi;
        UpdateGraphics();
    }

    // Terrain Object has integral coordinates.
    mPosition.Xf = 0.0f;
    mPosition.Yf = 0.0f;

    // TODO: maybe prefetch the entire area in the terrain factory.
    // mXMin = mPosition.X - std::min(mPosition.X, mRadius);
    // mYMin = mPosition.Y - std::min(mPosition.Y, mRadius);
    // mXMax = mPosition.X + std::min(std::numeric_limits<uint32_t>::max() - mPosition.X, mRadius);
    // mYMax = mPosition.Y + std::min(std::numeric_limits<uint32_t>::max() - mPosition.Y, mRadius);
}

void TerrainObject::UpdateGraphics()
{
    // Update all the cells around the center position.
    for (int32_t lY = -static_cast<int32_t>(mRadius); lY <= static_cast<int32_t>(mRadius); lY++)
    {
        for (int32_t lX = -static_cast<int32_t>(mRadius); lX <= static_cast<int32_t>(mRadius); lX++)
        {
            // Check whether the cell is inside the world bounds.
            // TODO: Check if position + offset is within world coord bounds instead of assuming the range of a coordinate is the whole uint32_t domain.
            if (!mPosition.TranslatedOverflow(lX, lY))
            {
                const Core::Terrain& lTerrain = mFactory.GetTerrain(mPosition.Xi + lX, mPosition.Yi + lY);
                mVertexArray.SetColor(mRadius + lX, mRadius + lY, static_cast<float>(lTerrain.Index));
            }
            else
            {
                mVertexArray.SetColor(mRadius + lX, mRadius + lY, 0);
            }
        }
    }
    mVertexArray.Update();
}

void TerrainObject::Draw()
{
    mVertexArray.Draw();
}

Core::WorldCoord TerrainObject::GetPosition() const
{
    return mPosition;
}

}