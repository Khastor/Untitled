#ifndef CORE_CHARACTER_VERTEX_ARRAY_HPP_INCLUDED
#define CORE_CHARACTER_VERTEX_ARRAY_HPP_INCLUDED

#include <glad/gl.h>

namespace Core
{

class CharacterVertexArray
{
public:
    CharacterVertexArray(
        float pR,
        float pG,
        float pB);

    ~CharacterVertexArray();

    void Draw();

private:
    GLuint mBufferHandle;
    GLuint mObjectHandle;
};

}

#endif // CORE_CHARACTER_VERTEX_ARRAY_HPP_INCLUDED
