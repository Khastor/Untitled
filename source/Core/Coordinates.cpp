#include <Core/Coordinates.hpp>
#include <Core/Numeric.hpp>

#include <algorithm>
#include <cmath>
#include <limits>

namespace Core
{

/**
 * Implementation details.
 */
namespace
{

const uint32_t kCoordMaxI = std::numeric_limits<uint32_t>::max();
const uint32_t kCoordMinI = std::numeric_limits<uint32_t>::min();
const float kCoordMaxF = std::nextafter(1.0f, 0.0f);
const float kCoordMinF = 0.0f;

/**
 * @brief Helper function to translate a composite world coordinate by a floating-point offset.
 *
 * The resulting coordinate is clamped to the world coordinates bounds.
 *
 * @param pCoordI Integral part of the composite world coordinate.
 * @param pCoordF Fractional part of the composite world coordinate.
 * @param pOffset Floating-point translation offset.
 */
void FractionalClampedTranslate(
    uint32_t& pCoordI,
    float& pCoordF,
    float pOffset)
{
    if (pOffset == 0.0f)
    {
        return;
    }

    pCoordF += pOffset;
    if (!Core::NumericGuard<uint32_t>::Overflow(pCoordI, std::floor(pCoordF)))
    {
        pCoordI += static_cast<uint32_t>(std::floor(pCoordF));
        if ((pCoordF = std::fmod(pCoordF, 1.0f)) < 0.0f)
        {
            pCoordF = std::fmod(pCoordF + 1.0f, 1.0f);
        }
    }
    else if (pOffset > 0.0f)
    {
        pCoordI = kCoordMaxI;
        pCoordF = kCoordMaxF;
    }
    else if (pOffset < 0.0f)
    {
        pCoordI = kCoordMinI;
        pCoordF = kCoordMinF;
    }
}

/**
 * @brief Helper function to translate a composite world coordinate by an integral offset.
 *
 * The resulting coordinate is clamped to the world coordinate bounds.
 *
 * @param pCoordI Integral part of the composite world coordinate.
 * @param pOffset Integral translation offset.
 */
void IntegralClampedTranslate(
    uint32_t& pCoordI,
    int32_t pOffset)
{
    if (pOffset == 0)
    {
        return;
    }

    if (!Core::NumericGuard<uint32_t>::Overflow(pCoordI, pOffset))
    {
        pCoordI += pOffset;
    }
    else if (pOffset > 0)
    {
        pCoordI = kCoordMaxI;
    }
    else if (pOffset < 0)
    {
        pCoordI = kCoordMinI;
    }
}

}

WorldCoord& WorldCoord::Center()
{
    Xf = 0.5f;
    Yf = 0.5f;
    return *this;
}

WorldCoord& WorldCoord::Center(
    Core::Direction pAxis)
{
    if (pAxis == Core::Direction::Horizontal)
    {
        Yf = 0.5f;
    }
    if (pAxis == Core::Direction::Vertical)
    {
        Xf = 0.5f;
    }
    return *this;
}

WorldCoord WorldCoord::Centered() const
{
    WorldCoord lCenteredCopy(*this);
    return lCenteredCopy.Center();
}

WorldCoord WorldCoord::Centered(
    Core::Direction pAxis) const
{
    WorldCoord lCenteredCopy(*this);
    return lCenteredCopy.Center(pAxis);
}

WorldCoord& WorldCoord::Clamp(
    const WorldCoord& pMinCoord,
    const WorldCoord& pMaxCoord)
{
    if (Xi < pMinCoord.Xi)
    {
        Xi = pMinCoord.Xi;
        Xf = pMinCoord.Xf;
    }
    else if (Xi == pMinCoord.Xi)
    {
        Xf = std::max(Xf, pMinCoord.Xf);
    }
    if (Xi > pMaxCoord.Xi)
    {
        Xi = pMaxCoord.Xi;
        Xf = pMaxCoord.Xf;
    }
    else if (Xi == pMaxCoord.Xi)
    {
        Xf = std::min(Xf, pMaxCoord.Xf);
    }
    if (Yi < pMinCoord.Yi)
    {
        Yi = pMinCoord.Yi;
        Yf = pMinCoord.Yf;
    }
    else if (Yi == pMinCoord.Yi)
    {
        Yf = std::max(Yf, pMinCoord.Yf);
    }
    if (Yi > pMaxCoord.Yi)
    {
        Yi = pMaxCoord.Yi;
        Yf = pMaxCoord.Yf;
    }
    else if (Yi == pMaxCoord.Yi)
    {
        Yf = std::min(Yf, pMaxCoord.Yf);
    }
    return *this;
}

WorldCoord WorldCoord::Clamped(
    const WorldCoord& pMinCoord,
    const WorldCoord& pMaxCoord) const
{
    WorldCoord lClampedCopy(*this);
    return lClampedCopy.Clamp(pMinCoord, pMaxCoord);
}

WorldCoord& WorldCoord::Translate(
    float pOffsetX,
    float pOffsetY)
{
    FractionalClampedTranslate(Xi, Xf, pOffsetX);
    FractionalClampedTranslate(Yi, Yf, pOffsetY);
    return *this;
}

WorldCoord& WorldCoord::Translate(
    int32_t pOffsetX,
    int32_t pOffsetY)
{
    IntegralClampedTranslate(Xi, pOffsetX);
    IntegralClampedTranslate(Yi, pOffsetY);
    return *this;
}

WorldCoord WorldCoord::Translated(
    float pOffsetX,
    float pOffsetY) const
{
    WorldCoord lTranslatedCopy(*this);
    return lTranslatedCopy.Translate(pOffsetX, pOffsetY);
}

WorldCoord WorldCoord::Translated(
    int32_t pOffsetX,
    int32_t pOffsetY) const
{
    WorldCoord lTranslatedCopy(*this);
    return lTranslatedCopy.Translate(pOffsetX, pOffsetY);
}

bool WorldCoord::TranslatedOverflow(
    float pOffsetX,
    float pOffsetY) const
{
    return Core::NumericGuard<uint32_t>::Overflow(Xi, std::floor(Xf + pOffsetX))
        || Core::NumericGuard<uint32_t>::Overflow(Yi, std::floor(Yf + pOffsetY));
}

bool WorldCoord::TranslatedOverflow(
    int32_t pOffsetX,
    int32_t pOffsetY) const
{
    return Core::NumericGuard<uint32_t>::Overflow(Xi, pOffsetX)
        || Core::NumericGuard<uint32_t>::Overflow(Yi, pOffsetY);
}

bool operator==(
    const Core::WorldCoord& pLhs,
    const Core::WorldCoord& pRhs)
{
    return pLhs.Xi == pRhs.Xi && pLhs.Yi == pRhs.Yi && pLhs.Xf == pRhs.Xf && pLhs.Yf == pRhs.Yf;
}

bool operator!=(
    const Core::WorldCoord& pLhs,
    const Core::WorldCoord& pRhs)
{
    return !(pLhs == pRhs);
}

uint32_t ManhattanDistance(
    const Core::WorldCoord& pLhs,
    const Core::WorldCoord& pRhs)
{
    const uint32_t lDistanceX = std::max(pLhs.Xi, pRhs.Xi) - std::min(pLhs.Xi, pRhs.Xi);
    const uint32_t lDistanceY = std::max(pLhs.Yi, pRhs.Yi) - std::min(pLhs.Yi, pRhs.Yi);
    return lDistanceX + lDistanceY;
}

float ManhattanDistance(
    const Core::LocalCoordF& pLhs,
    const Core::LocalCoordF& pRhs)
{
    return std::abs(pLhs.X - pRhs.X) + std::abs(pLhs.Y - pRhs.Y);
}

float EuclideanDistance(
    const Core::LocalCoordF& pLhs,
    const Core::LocalCoordF& pRhs)
{
    return std::hypot(pLhs.X - pRhs.X, pLhs.Y - pRhs.Y);
}

WorldFrame WorldFrame::CreateFromCenter(
    const WorldCoord& pCoord,
    float pHorizontalHalfSize,
    float pVerticalHalfSize)
{
    const WorldCoord lMinCoord{ kCoordMinI, kCoordMinI, kCoordMinF , kCoordMinF };
    const WorldCoord lMaxCoord{ kCoordMaxI, kCoordMaxI, kCoordMaxF , kCoordMaxF };
    const WorldCoord lFrameCenter = pCoord.Clamped(
        lMinCoord.Translated(pHorizontalHalfSize, pVerticalHalfSize),
        lMaxCoord.Translated(-pHorizontalHalfSize, -pVerticalHalfSize));

    return WorldFrame{
        lFrameCenter.Translated(-pHorizontalHalfSize, -pVerticalHalfSize),
        lFrameCenter.Translated(pHorizontalHalfSize, pVerticalHalfSize)
    };
}

}
