#include <Core/CharacterVertexArray.hpp>

namespace Core
{

CharacterVertexArray::CharacterVertexArray(
    float pR,
    float pG,
    float pB) :
    mObjectHandle(0),
    mBufferHandle(0)
{
    GLfloat lBufferData[] =
    {
        -0.5f,  0.5f, pR, pG, pB,
         0.5f,  0.5f, pR, pG, pB,
        -0.5f, -0.5f, pR, pG, pB,
        -0.5f, -0.5f, pR, pG, pB,
         0.5f,  0.5f, pR, pG, pB,
         0.5f, -0.5f, pR, pG, pB
    };

    glCreateVertexArrays(1, &mObjectHandle);
    glCreateBuffers(1, &mBufferHandle);
    glNamedBufferStorage(mBufferHandle, 6 * 5 * sizeof(GLfloat), lBufferData, GL_DYNAMIC_STORAGE_BIT);
    glVertexArrayVertexBuffer(mObjectHandle, 0, mBufferHandle, 0, 5 * sizeof(GLfloat));

    glEnableVertexArrayAttrib(mObjectHandle, 0);
    glVertexArrayAttribFormat(mObjectHandle, 0, 2, GL_FLOAT, GL_FALSE, 0);
    glVertexArrayAttribBinding(mObjectHandle, 0, 0);

    glEnableVertexArrayAttrib(mObjectHandle, 1);
    glVertexArrayAttribFormat(mObjectHandle, 1, 3, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat));
    glVertexArrayAttribBinding(mObjectHandle, 1, 0);
}

CharacterVertexArray::~CharacterVertexArray()
{
    glDeleteVertexArrays(1, &mObjectHandle);
    glDeleteBuffers(1, &mBufferHandle);
}

void CharacterVertexArray::Draw()
{
    glBindVertexArray(mObjectHandle);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);
}

}
