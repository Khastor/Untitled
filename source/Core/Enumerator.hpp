#ifndef CORE_ENUMERATOR_HPP_INCLUDED
#define CORE_ENUMERATOR_HPP_INCLUDED

#include <cstdint>
#include <limits>
#include <type_traits>

namespace Core
{

/**
 * @brief Enumerator for a sequence of enum flags.
 *
 * Use this enumerator in range-for loops to iterate through a sequence of enum flags.
 * Iterate through all flag values from 1 << First to 1 << Last included.
 *
 * @tparam EnumT The enum flags type.
 * @tparam First The index of the first enum flag in the sequence.
 * @tparam Last The index of the last enum flag in the sequence.
 */
template<typename EnumT, uint8_t First, uint8_t Last>
struct FlagsEnumerator
{
    static_assert(std::is_enum_v<EnumT>, "EnumT must be an enum type");

    struct Iterator
    {
        EnumT operator*() const
        {
            return static_cast<EnumT>(std::underlying_type_t<EnumT>(1) << Index);
        }

        Iterator& operator++()
        {
            Index++;
            return *this;
        }

        bool operator!=(
            const Iterator& pOther) const
        {
            return Index != pOther.Index;
        }

        uint8_t Index;
    };

    Iterator begin() const
    {
        return Iterator{ First };
    }

    Iterator end() const
    {
        return Iterator{ Last + 1 };
    }
};

/**
 * @brief Enumerator for a sequence of enum values.
 *
 * Use this enumerator in range-for loops to iterate through a sequence of enum values.
 * Iterate through all values from First to Last included.
 *
 * @tparam EnumT The enum type.
 * @tparam First The first enum value in the sequence.
 * @tparam Last The last enum value in the sequence.
 */
template<typename EnumT, EnumT First, EnumT Last>
struct ValuesEnumerator
{
    static_assert(std::is_enum_v<EnumT>, "EnumT must be an enum type");

    static constexpr EnumT Max{ std::numeric_limits<std::underlying_type_t<EnumT>>::max() };
    static_assert(First <= Last && First < Max && Last < Max);

    static constexpr EnumT Next(
        EnumT pValue)
    {
        return static_cast<EnumT>(static_cast<std::underlying_type_t<EnumT>>(pValue) + 1);
    }

    struct Iterator
    {
        EnumT operator*() const
        {
            return Value;
        }

        Iterator& operator++()
        {
            Value = Next(Value);
            return *this;
        }

        bool operator!=(
            const Iterator& pOther) const
        {
            return Value != pOther.Value;
        }

        EnumT Value;
    };

    Iterator begin() const
    {
        return Iterator{ First };
    }

    Iterator end() const
    {
        return Iterator{ Next(Last) };
    }
};

}

#endif // CORE_ENUMERATOR_HPP_INCLUDED
