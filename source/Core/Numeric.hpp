#ifndef CORE_NUMERIC_HPP_INCLUDED
#define CORE_NUMERIC_HPP_INCLUDED

#include <type_traits>

namespace Core
{

/**
 * @brief Numeric guards for operations on unsigned integers.
 *
 * @tparam IntegerT Unsigned integer type.
 * @tparam Implementation detail to enforce type constraints on IntegerT.
 */
template<typename IntegerT, typename = std::enable_if_t<std::is_integral_v<IntegerT> && std::is_unsigned_v<IntegerT>>>
struct NumericGuard
{
    /**
     * @brief Corresponding signed integer type, used as an offset.
     */
    using OffsetT = std::make_signed_t<IntegerT>;

    /**
     * @brief Check for unsigned integer overflow when adding an offset.
     *
     * This guard checks that the result of adding an offset to an unsigned integer value does not overflow.
     *
     * @param pValue Initial value.
     * @param pOffset Offset value.
     * @return true if the operation would result in an unsigned integer overflow.
     */
    static constexpr bool Overflow(
        IntegerT pValue,
        OffsetT pOffset)
    {
        const IntegerT lResult = pValue + pOffset;
        return (pOffset >= 0) ? !(lResult >= pValue) : !(lResult < pValue);
    }

};

}

#endif // CORE_NUMERIC_HPP_INCLUDED
