#ifndef CORE_DIRECTION_HPP_INCLUDED
#define CORE_DIRECTION_HPP_INCLUDED

#include <Core/Enumerator.hpp>

#include <cstdint>
#include <type_traits>

namespace Core
{

/**
 * @brief Cardinal direction.
 */
enum class Direction : uint8_t
{
    None,

    North,
    South,
    East,
    West,

    Horizontal,
    Vertical
};

/**
 * @brief Cardinal direction flags.
 */
enum class DirectionFlags : uint8_t
{
    None        = 0,

    North       = 1 << 0,
    South       = 1 << 1,
    East        = 1 << 2,
    West        = 1 << 3,

    Horizontal  = East | West,
    Vertical    = North | South
};

inline constexpr Core::DirectionFlags operator|(
    const Core::DirectionFlags& pLeft,
    const Core::DirectionFlags& pRight)
{
    return static_cast<Core::DirectionFlags>(
        static_cast<std::underlying_type_t<Core::DirectionFlags>>(pLeft) |
        static_cast<std::underlying_type_t<Core::DirectionFlags>>(pRight));
}

inline constexpr Core::DirectionFlags& operator|=(
    Core::DirectionFlags& pLeft,
    const Core::DirectionFlags& pRight)
{
    return pLeft = pLeft | pRight;
}

inline constexpr Core::DirectionFlags operator&(
    const Core::DirectionFlags& pLeft,
    const Core::DirectionFlags& pRight)
{
    return static_cast<Core::DirectionFlags>(
        static_cast<std::underlying_type_t<Core::DirectionFlags>>(pLeft) &
        static_cast<std::underlying_type_t<Core::DirectionFlags>>(pRight));
}

inline constexpr Core::DirectionFlags& operator&=(
    Core::DirectionFlags& pLeft,
    const Core::DirectionFlags& pRight)
{
    return pLeft = pLeft & pRight;
}

/**
 * @brief Cardinal direction helpers.
 */
struct DirectionHelper
{
    /**
     * @brief Count the number of directions.
     */
    static constexpr uint8_t Count(
        Core::DirectionFlags pDirections)
    {
        uint8_t lCount{};
        lCount += ((pDirections & Core::DirectionFlags::North) == Core::DirectionFlags::North);
        lCount += ((pDirections & Core::DirectionFlags::South) == Core::DirectionFlags::South);
        lCount += ((pDirections & Core::DirectionFlags::East) == Core::DirectionFlags::East);
        lCount += ((pDirections & Core::DirectionFlags::West) == Core::DirectionFlags::West);
        return lCount;
    }

    /**
     * @brief Horizontal unit offset of a direction.
     */
    static constexpr int32_t HorizontalOffset(
        Core::DirectionFlags pDirections)
    {
        int32_t lOffset{};
        lOffset += 1 * ((pDirections & Core::DirectionFlags::East) == Core::DirectionFlags::East);
        lOffset -= 1 * ((pDirections & Core::DirectionFlags::West) == Core::DirectionFlags::West);
        return lOffset;
    }

    /**
     * @brief Horizontal unit offset of a direction.
     */
    static constexpr int32_t HorizontalOffset(
        Core::Direction pDirection)
    {
        int32_t lOffset{};
        lOffset += 1 * (pDirection == Core::Direction::East);
        lOffset -= 1 * (pDirection == Core::Direction::West);
        return lOffset;
    }

    /**
     * @brief Vertical unit offset of a direction.
     */
    static constexpr int32_t VerticalOffset(
        Core::DirectionFlags pDirections)
    {
        int32_t lOffset{};
        lOffset += 1 * ((pDirections & Core::DirectionFlags::North) == Core::DirectionFlags::North);
        lOffset -= 1 * ((pDirections & Core::DirectionFlags::South) == Core::DirectionFlags::South);
        return lOffset;
    }

    /**
     * @brief Vertical unit offset of a direction.
     */
    static constexpr int32_t VerticalOffset(
        Core::Direction pDirection)
    {
        int32_t lOffset{};
        lOffset += 1 * (pDirection == Core::Direction::North);
        lOffset -= 1 * (pDirection == Core::Direction::South);
        return lOffset;
    }

    /**
     * @brief Get the axis of a direction.
     */
    static constexpr Direction Axis(
        Core::Direction pDirection)
    {
        switch (pDirection)
        {
        case Core::Direction::North:
        case Core::Direction::South:
        case Core::Direction::Vertical:
            return Core::Direction::Vertical;
        case Core::Direction::East:
        case Core::Direction::West:
        case Core::Direction::Horizontal:
            return Core::Direction::Horizontal;
        default:
            return Core::Direction::None;
        }
        return Core::Direction::None;
    }

    /**
     * @brief Get the opposite direction.
     *
     * @param pDirection The cardinal direction.
     * @return The opposite cardinal direction.
     */
    static constexpr Core::Direction Opposite(
        Core::Direction pDirection)
    {
        switch (pDirection)
        {
        case Core::Direction::North:
            return Core::Direction::South;
        case Core::Direction::South:
            return Core::Direction::North;
        case Core::Direction::East:
            return Core::Direction::West;
        case Core::Direction::West:
            return Core::Direction::East;
        default:
            return Core::Direction::None;
        }
        return Core::Direction::None;
    }

    /**
     * @brief Enumerate direction flags.
     */
    using DirectionFlagsEnumerator = Core::FlagsEnumerator<Core::DirectionFlags, 0, 3>;

    /**
     * @brief Enumerate direction values.
     */
    using DirectionValuesEnumerator = Core::ValuesEnumerator<Core::Direction, Core::Direction::North, Core::Direction::West>;
};

}

#endif // CORE_DIRECTION_HPP_INCLUDED
