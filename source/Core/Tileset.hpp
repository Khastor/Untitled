#ifndef CORE_TILESET_HPP_INCLUDED
#define CORE_TILESET_HPP_INCLUDED

#include <Core/Direction.hpp>

#include <cstdint>

namespace Core
{

inline uint8_t GetTilesetTextureIndex(
    DirectionFlags pDirections)
{
    static uint8_t sIndices[16] =
    {
        0,  // Direction = None
        0,  // Direction = North
        0,  // Direction = South
        11, // Direction = North | South
        0,  // Direction = East
        6,  // Direction = North | East
        7,  // Direction = South | East
        3,  // Direction = North | South | East
        0,  // Direction = West
        9,  // Direction = North | West
        8,  // Direction = South | West
        5,  // Direction = North | South | West
        10, // Direction = East | West
        2,  // Direction = North | East | West
        4,  // Direction = South | East | West
        1,  // Direction = North | South | East | West
    };
    return sIndices[static_cast<uint8_t>(pDirections)];
}

}

#endif // CORE_TILESET_HPP_INCLUDED
