#ifndef CORE_COORDINATES_HPP_INCLUDED
#define CORE_COORDINATES_HPP_INCLUDED

#include <Core/Direction.hpp>

#include <algorithm>
#include <cassert>
#include <cstdint>
#include <limits>

namespace Core
{

/**
 * @brief Composite world coordinates of a point in the world.
 *
 * Separate the integral and fractional parts of the coordinates to avoid precision issues.
 * The integral coordinates give the position of a cell in the world, and range between 0 and UINT_MAX.
 * The fractional coordinates give the position of a point in the cell, and range between 0.0f and 1.0f.
 */
struct WorldCoord
{
    /**
     * @brief Center the world coordinates.
     *
     * The world coordinates are centered on the cell.
     *
     * @return The centered world coordinates.
     */
    WorldCoord& Center();

    /**
     * @brief Center the world coordinates along an axis.
     *
     * The world coordinates are centered on the cell along the axis.
     *
     * @param pAxis The axis along which to center the coordinates.
     * @return The centered world coordinates.
     */
    WorldCoord& Center(
        Core::Direction pAxis);

    /**
     * @brief Get a centered copy of the world coordinates.
     *
     * The world coordinates are centered on the cell.
     *
     * @return The centered world coordinates.
     */
    WorldCoord Centered() const;

    /**
     * @brief Get a centered copy of the world coordinates along an axis.
     *
     * The world coordinates are centered on the cell along the axis.
     *
     * @param pAxis The axis along which to center the coordinates.
     * @return The centered world coordinates.
     */
    WorldCoord Centered(
        Core::Direction pAxis) const;

    /**
     * @brief Clamp the world coordinates between two bounds.
     *
     * @param pMinCoord The min world coordinates bound.
     * @param pMaxCoord The max world coordinates bound.
     * @return The clamped world coordinates.
     */
    WorldCoord& Clamp(
        const WorldCoord& pMinCoord,
        const WorldCoord& pMaxCoord);

    /**
     * @brief Get a clamped copy of the world coordinates between two bounds.
     *
     * @param pMinCoord The min world coordinates bound.
     * @param pMaxCoord The max world coordinates bound.
     * @return The clamped copy of the world coordinates.
     */
    WorldCoord Clamped(
        const WorldCoord& pMinCoord,
        const WorldCoord& pMaxCoord) const;

    /**
     * @brief Translate the world coordinates.
     *
     * Both the integral and fractional parts of the coordinates are translated accordingly.
     * If the translated coordinates would overflow, they are clamped to valid world coordinates.
     *
     * @param pOffsetX The translation offset on the X axis.
     * @param pOffsetY The translation offset on the Y axis.
     * @return The translated world coordinates.
     */
    WorldCoord& Translate(
        float pOffsetX,
        float pOffsetY);

    /**
     * @brief Translate the world coordinates.
     *
     * Only the integral part of the coordinates is translated.
     * If the translated coordinates would overflow, they are clamped to valid world coordinates.
     *
     * @param pOffsetX The translation offset on the X axis.
     * @param pOffsetY The translation offset on the Y axis.
     * @return The translated world coordinates.
     */
    WorldCoord& Translate(
        int32_t pOffsetX,
        int32_t pOffsetY);

    /**
     * @brief Get a translated copy of the world coordinates.
     *
     * Both the integral and fractional parts of the coordinates are translated accordingly.
     * If the translated coordinates would overflow, they are clamped to valid world coordinates.
     *
     * @param pOffsetX The translation offset on the X axis.
     * @param pOffsetY The translation offset on the Y axis.
     * @return The translated copy of the world coordinates.
     */
    WorldCoord Translated(
        float pOffsetX,
        float pOffsetY) const;

    /**
     * @brief Get a translated copy of the world coordinates.
     *
     * Only the integral part of the coordinates is translated.
     * If the translated coordinates would overflow, they are clamped to valid world coordinates.
     *
     * @param pOffsetX The translation offset on the X axis.
     * @param pOffsetY The translation offset on the Y axis.
     * @return The translated copy of the world coordinates.
     */
    WorldCoord Translated(
        int32_t pOffsetX,
        int32_t pOffsetY) const;

    /**
     * @brief Check for translated world coordinates overflow.
     *
     * Both the integral and fractional parts of the coordinates are translated accordingly.
     *
     * @param pOffsetX The translation offset on the X axis.
     * @param pOffsetY The translation offset on the Y axis.
     * @return true if the translated coordinates would overflow.
     */
    bool TranslatedOverflow(
        float pOffsetX,
        float pOffsetY) const;

    /**
     * @brief Check for translated world coordinates overflow.
     *
     * Only the integral part of the coordinates is translated.
     *
     * @param pOffsetX The translation offset on the X axis.
     * @param pOffsetY The translation offset on the Y axis.
     * @return true if the translated coordinates would overflow.
     */
    bool TranslatedOverflow(
        int32_t pOffsetX,
        int32_t pOffsetY) const;

    uint32_t Xi;
    uint32_t Yi;
    float Xf;
    float Yf;
};

bool operator==(
    const Core::WorldCoord& pLhs,
    const Core::WorldCoord& pRhs);

bool operator!=(
    const Core::WorldCoord& pLhs,
    const Core::WorldCoord& pRhs);

uint32_t ManhattanDistance(
    const Core::WorldCoord& pLhs,
    const Core::WorldCoord& pRhs);

/**
 * @brief Composite local coordinates of a point in the world, relative to a local view.
 *
 * Separate the integral part and the fractional part of the coordinates to avoid precision issues.
 * The integral coordinates denote the position of a cell, relative to a local view.
 * The fractional coordinates denote the position of a point in the cell, normalized between 0 and 1.
 * Local coordinates are read-only.
 */
struct LocalCoord
{
    const uint32_t Xi;
    const uint32_t Yi;
    const float Xf;
    const float Yf;
};

/**
 * @brief Floating-point local coordinates of a point in the world, relative to a local view.
 *
 * The coordinates of the point in the local view are small enough to avoid precision issues.
 * Local coordinates are read-only.
 */
struct LocalCoordF
{
    LocalCoordF(
        const LocalCoord& pCoord) :
        X(pCoord.Xi + pCoord.Xf),
        Y(pCoord.Yi + pCoord.Yf)
    {}

    const float X;
    const float Y;
};

float ManhattanDistance(
    const Core::LocalCoordF& pLhs,
    const Core::LocalCoordF& pRhs);

float EuclideanDistance(
    const Core::LocalCoordF& pLhs,
    const Core::LocalCoordF& pRhs);

/**
 * @brief Rectangular frame with composite world coordinates.
 *
 * Min is the bottom-left frame corner, Max is the top-right frame corner.
 */
struct WorldFrame
{
    static WorldFrame CreateFromCenter(
        const WorldCoord& pCoord,
        float pHorizontalHalfSize,
        float pVerticalHalfSize);

    WorldCoord Min;
    WorldCoord Max;
};

/**
 * @brief Rectangular frame with composite local coordinates, relative to a local view.
 *
 * Min is the bottom-left frame corner, Max is the top-right frame corner.
 */
struct LocalFrame
{
    const LocalCoord Min;
    const LocalCoord Max;
};

/**
 * @brief Rectangular frame with floating-point local coordinates, relative to a local view.
 *
 * Min is the bottom-left frame corner, Max is the top-right frame corner.
 */
struct LocalFrameF
{
    LocalFrameF(
        const LocalFrame& pFrame) :
        Min(pFrame.Min),
        Max(pFrame.Max)
    {}

    const LocalCoordF Min;
    const LocalCoordF Max;
};

/**
 * @brief Local view on the world.
 */
struct LocalView
{
    LocalView(
        const WorldCoord& pCenter,
        uint32_t pHalfSize)
    {
        SetCenter(pCenter, pHalfSize);
    }

    void SetCenter(
        const WorldCoord& pCoord)
    {
        WorldCoord lCenter{};
        lCenter.Xi = std::clamp(pCoord.Xi, mHalfSize, std::numeric_limits<uint32_t>::max() - mHalfSize);
        lCenter.Yi = std::clamp(pCoord.Yi, mHalfSize, std::numeric_limits<uint32_t>::max() - mHalfSize);
        SetOrigin({ lCenter.Xi - mHalfSize, lCenter.Yi - mHalfSize, pCoord.Xf, pCoord.Yf });
    }

    void SetCenter(
        const WorldCoord& pCoord,
        uint32_t pHalfSize)
    {
        mHalfSize = pHalfSize;
        SetCenter(pCoord);
    }

    void SetOrigin(
        const WorldCoord& pCoord)
    {
        mOrigin.Xi = std::min(pCoord.Xi, std::numeric_limits<uint32_t>::max() - mHalfSize * 2);
        mOrigin.Yi = std::min(pCoord.Yi, std::numeric_limits<uint32_t>::max() - mHalfSize * 2);
        mOrigin.Xf = pCoord.Xf;
        mOrigin.Yf = pCoord.Yf;
    }

    void SetOrigin(
        const WorldCoord& pCoord,
        uint32_t pHalfSize)
    {
        mHalfSize = pHalfSize;
        SetOrigin(pCoord);
    }

    bool Contains(
        const WorldCoord& pCoord) const
    {
        return (pCoord.Xi >= mOrigin.Xi && pCoord.Xi <= mOrigin.Xi + mHalfSize * 2)
            && (pCoord.Yi >= mOrigin.Yi && pCoord.Yi <= mOrigin.Yi + mHalfSize * 2);
    }

    bool Contains(
        const WorldFrame& pFrame) const
    {
        return Contains(pFrame.Min) && Contains(pFrame.Max);
    }

    /**
     * @brief Map world coordinates to local view coordinates.
     *
     * The world coordinates must be contained in the view.
     *
     * @param pCoord The world coordinates to map to local view coordinates.
     * @return The corresponding local view coordinates.
     */
    LocalCoord MapToLocal(
        const WorldCoord& pCoord) const
    {
        // Check pre-conditions.
        assert(Contains(pCoord));

        return LocalCoord{ pCoord.Xi - mOrigin.Xi, pCoord.Yi - mOrigin.Yi, pCoord.Xf, pCoord.Yf };
    }

    /**
     * @brief Map a frame in world coordinates to local view coordinates.
     *
     * The frame must be contained in the view.
     *
     * @param pFrame The frame in world coordinates to map to local view coordinates.
     * @return The corresponding frame in local view coordinates.
     */
    LocalFrame MapToLocal(
        const WorldFrame& pFrame) const
    {
        // Check pre-conditions.
        assert(Contains(pFrame));

        return LocalFrame{ MapToLocal(pFrame.Min), MapToLocal(pFrame.Max) };
    }

    WorldCoord mOrigin;
    uint32_t mHalfSize;
};

}

#endif // CORE_COORDINATES_HPP_INCLUDED
