#ifndef CORE_CAMERA_HPP_INCLUDED
#define CORE_CAMERA_HPP_INCLUDED

#include <Core/Coordinates.hpp>

#include <cmath>

namespace Core
{

struct Camera
{
    void SetAspectRatio(
        float pRatio);

    void SetZoom(
        float pZoom);

    float GetZoom() const;

    void SetPosition(
        const Core::WorldCoord& pPosition);

    Core::WorldCoord GetPosition() const;

    void SetTargetPosition(
        const Core::WorldCoord& pPosition);

    Core::WorldFrame GetFrame() const;

    Core::LocalView GetLocalView() const;

private:
    float mRatio;
    float mZoom;
    Core::WorldCoord mPosition;
};

}

#endif // CORE_CAMERA_HPP_INCLUDED
