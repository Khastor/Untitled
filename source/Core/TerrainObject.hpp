#ifndef CORE_TERRAIN_OBJECT_HPP_INCLUDED
#define CORE_TERRAIN_OBJECT_HPP_INCLUDED

#include <Core/Coordinates.hpp>
#include <Core/TerrainFactory.hpp>
#include <Core/TerrainVertexArray.hpp>

#include <cstdint>

namespace Core
{

class TerrainObject
{
public:
    TerrainObject(
        Core::TerrainFactory& pFactory,
        uint32_t pRadius);

    void CenterOn(
        const Core::WorldCoord& pPosition);

    void UpdateGraphics();

    void Draw();

    Core::WorldCoord GetPosition() const;

private:
    uint32_t mRadius;
    Core::WorldCoord mPosition;
    Core::TerrainFactory& mFactory;
    Core::TerrainVertexArray mVertexArray;
};

}

#endif // CORE_TERRAIN_OBJECT_HPP_INCLUDED
