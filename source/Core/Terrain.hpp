#ifndef CORE_TERRAIN_HPP_INCLUDED
#define CORE_TERRAIN_HPP_INCLUDED

#include <cstdint>

namespace Core
{

struct Terrain
{
    bool Walkable;
    uint8_t Index;
};

}

#endif // CORE_TERRAIN_HPP_INCLUDED
