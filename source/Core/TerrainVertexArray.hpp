#ifndef CORE_TERRAIN_VERTEX_ARRAY_HPP_INCLUDED
#define CORE_TERRAIN_VERTEX_ARRAY_HPP_INCLUDED

#include <glad/gl.h>

#include <cstddef>
#include <cstdint>
#include <cmath>
#include <vector>

namespace Core
{

template<typename ValueT>
struct Alignment
{
    static constexpr size_t Offset(size_t pIndex)
    {
        return (sizeof(ValueT) - pIndex % sizeof(ValueT)) % sizeof(ValueT);
    }

    static constexpr size_t Index(size_t pIndex)
    {
        return pIndex + Offset(pIndex);
    }
};

struct TerrainVertexArray
{
    TerrainVertexArray(uint32_t pSize) : mSize(pSize)
    {
        mVertexBufferData.clear();

        std::vector<GLuint> lIndexBuffer;
        constexpr uint32_t lQuadIndices = 6;
        constexpr uint32_t lQuadVertices = 4;
        constexpr uint32_t lQuadVertexCoordAttributeSize = 2;
        constexpr uint32_t lQuadTextureCoordAttributeSize = 3;
        const uint32_t lBufferStride = lQuadVertexCoordAttributeSize + lQuadTextureCoordAttributeSize;
        const float lOffset = 0.5f * static_cast<float>(pSize);
        for (uint32_t lQuadY = 0; lQuadY < pSize; lQuadY++)
        {
            for (uint32_t lQuadX = 0; lQuadX < pSize; lQuadX++)
            {
                const uint32_t lQuadIndex = mVertexBufferData.size() / lBufferStride;

                lIndexBuffer.push_back(lQuadIndex);
                lIndexBuffer.push_back(lQuadIndex + 1);
                lIndexBuffer.push_back(lQuadIndex + 2);
                lIndexBuffer.push_back(lQuadIndex);
                lIndexBuffer.push_back(lQuadIndex + 3);
                lIndexBuffer.push_back(lQuadIndex + 2);

                const float lVertexX = -lOffset + static_cast<float>(lQuadX);
                const float lVertexY = -lOffset + static_cast<float>(lQuadY);

                mVertexBufferData.push_back(lVertexX);
                mVertexBufferData.push_back(lVertexY);
                mVertexBufferData.push_back(0.0f);
                mVertexBufferData.push_back(1.0f);
                mVertexBufferData.push_back(0.0f);

                mVertexBufferData.push_back(lVertexX + 1.0f);
                mVertexBufferData.push_back(lVertexY);
                mVertexBufferData.push_back(1.0f);
                mVertexBufferData.push_back(1.0f);
                mVertexBufferData.push_back(0.0f);

                mVertexBufferData.push_back(lVertexX + 1.0f);
                mVertexBufferData.push_back(lVertexY + 1.0f);
                mVertexBufferData.push_back(1.0f);
                mVertexBufferData.push_back(0.0f);
                mVertexBufferData.push_back(0.0f);

                mVertexBufferData.push_back(lVertexX);
                mVertexBufferData.push_back(lVertexY + 1.0f);
                mVertexBufferData.push_back(0.0f);
                mVertexBufferData.push_back(0.0f);
                mVertexBufferData.push_back(0.0f);

            }
        }

        mVertexBufferSize = sizeof(GLfloat) * mVertexBufferData.size();
        mIndexBufferOffset = Alignment<GLuint>::Index(mVertexBufferSize);

        const size_t lIndexBufferSize = sizeof(GLuint) * lIndexBuffer.size();
        const size_t lBufferSize = mIndexBufferOffset + lIndexBufferSize;

        glCreateVertexArrays(1, &mObjectHandle);
        glCreateBuffers(1, &mBufferHandle);
        glNamedBufferStorage(mBufferHandle, lBufferSize, nullptr, GL_DYNAMIC_STORAGE_BIT);
        glVertexArrayVertexBuffer(mObjectHandle, 0, mBufferHandle, 0, lBufferStride * sizeof(GLfloat));

        glEnableVertexArrayAttrib(mObjectHandle, 0);
        glVertexArrayAttribFormat(mObjectHandle, 0, lQuadVertexCoordAttributeSize, GL_FLOAT, GL_FALSE, 0);
        glVertexArrayAttribBinding(mObjectHandle, 0, 0);

        glEnableVertexArrayAttrib(mObjectHandle, 1);
        glVertexArrayAttribFormat(mObjectHandle, 1, lQuadTextureCoordAttributeSize, GL_FLOAT, GL_FALSE, lQuadVertexCoordAttributeSize * sizeof(GLfloat));
        glVertexArrayAttribBinding(mObjectHandle, 1, 0);

        glVertexArrayElementBuffer(mObjectHandle, mBufferHandle);

        // Upload index data
        glNamedBufferSubData(mBufferHandle, mIndexBufferOffset, lIndexBufferSize, lIndexBuffer.data());
    }

    void SetColor(uint32_t pQuadX, uint32_t pQuadY, float pColor)
    {
        uint32_t pIndex = pQuadX + pQuadY * mSize;
        mVertexBufferData[pIndex * 4 * 5 + 4] = pColor;
        mVertexBufferData[pIndex * 4 * 5 + 4 + 5] = pColor;
        mVertexBufferData[pIndex * 4 * 5 + 4 + 10] = pColor;
        mVertexBufferData[pIndex * 4 * 5 + 4 + 15] = pColor;
    }

    void Update()
    {
        glNamedBufferSubData(mBufferHandle, 0, mVertexBufferSize, mVertexBufferData.data());
    }

    void Draw()
    {
        glBindVertexArray(mObjectHandle);
        glDrawElements(GL_TRIANGLES, 6 * mSize * mSize, GL_UNSIGNED_INT, (const void*)mIndexBufferOffset);
        glBindVertexArray(0);
    }

    ~TerrainVertexArray()
    {
        glDeleteVertexArrays(1, &mObjectHandle);
        glDeleteBuffers(1, &mBufferHandle);
    }

    GLuint mObjectHandle;
    GLuint mBufferHandle;
    std::vector<GLfloat> mVertexBufferData;
    size_t mIndexBufferOffset;
    size_t mVertexBufferSize;
    uint32_t mSize;
};

}

#endif // CORE_TERRAIN_VERTEX_ARRAY_HPP_INCLUDED
