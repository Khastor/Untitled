#include <Core/Camera.hpp>

namespace Core
{

namespace
{

constexpr float kMinCameraZoom{ 5.0f };
constexpr float kMaxCameraZoom{ 100.0f };

}

void Camera::SetAspectRatio(
    float pRatio)
{
    mRatio = pRatio;
}

void Camera::SetZoom(
    float pZoom)
{
    mZoom = std::clamp(pZoom, kMinCameraZoom, kMaxCameraZoom);
}

float Camera::GetZoom() const
{
    return mZoom;
}

void Camera::SetPosition(
    const Core::WorldCoord& pPosition)
{
    mPosition = pPosition;
}

Core::WorldCoord Camera::GetPosition() const
{
    return mPosition;
}

void Camera::SetTargetPosition(
    const Core::WorldCoord& pPosition)
{
    const Core::WorldFrame lCameraConstraint = Core::WorldFrame::CreateFromCenter(pPosition, mRatio * mZoom * 0.5f, mZoom * 0.5f);
    mPosition.Clamp(lCameraConstraint.Min, lCameraConstraint.Max);
}

Core::WorldFrame Camera::GetFrame() const
{
    return Core::WorldFrame::CreateFromCenter(mPosition, mRatio * mZoom, mZoom);
}

Core::LocalView Camera::GetLocalView() const
{
    return Core::LocalView(mPosition, std::ceil(mRatio * kMaxCameraZoom) * 2);
}

}
