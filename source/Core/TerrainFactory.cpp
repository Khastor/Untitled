#include <Core/Algorithms/DisjointSet.hpp>
#include <Core/Direction.hpp>
#include <Core/Numeric.hpp>
#include <Core/TerrainFactory.hpp>
#include <Core/Tileset.hpp>

#include <cstdint>
#include <random>
#include <vector>

namespace Core
{

/**
 * Implementation details.
 */
namespace
{

constexpr uint32_t kTerrainChunkSideNodes = 31;
constexpr uint32_t kTerrainChunkSideSize = kTerrainChunkSideNodes * 2 - 1;
constexpr uint32_t kTerrainChunkSideStride = kTerrainChunkSideSize + 1;

// TODO: refactor in Issue #16
struct TerrainNode
{
    struct Coord
    {
        uint32_t X;
        uint32_t Y;
    };

    static constexpr uint32_t LocalToGlobal(
        uint32_t pCoord)
    {
        return 2 * pCoord;
    }

    static constexpr Coord IndexToLocalCoord(
        uint32_t pIndex)
    {
        return Coord{
            pIndex % kTerrainChunkSideNodes,
            pIndex / kTerrainChunkSideNodes
        };
    }

    static constexpr Coord IndexToGlobalCoord(
        uint32_t pIndex)
    {
        return Coord{
            LocalToGlobal(pIndex % kTerrainChunkSideNodes),
            LocalToGlobal(pIndex / kTerrainChunkSideNodes)
        };
    }

    static constexpr bool IsLeaf(
        DirectionFlags pDirections)
    {
        return DirectionHelper::Count(pDirections) <= 1;
    }

    DirectionFlags Edges;
};

// TODO: refactor in Issue #16
struct TerrainEdge
{
    static constexpr DirectionFlags Direction(
        uint32_t pSourceNode,
        uint32_t pTargetNode)
    {
        TerrainNode::Coord lSourceCoord = TerrainNode::IndexToLocalCoord(pSourceNode);
        TerrainNode::Coord lTargetCoord = TerrainNode::IndexToLocalCoord(pTargetNode);
        if (lSourceCoord.X != lTargetCoord.X)
        {
            return (lSourceCoord.X > lTargetCoord.X) ? DirectionFlags::West : DirectionFlags::East;
        }
        if (lSourceCoord.Y != lTargetCoord.Y)
        {
            return (lSourceCoord.Y > lTargetCoord.Y) ? DirectionFlags::South : DirectionFlags::North;
        }
        return DirectionFlags::None;
    }

    uint32_t U;
    uint32_t V;
    bool Walkable;
};

// TODO: refactor in Issue #16
inline constexpr bool NextNodeIsInBounds(
    uint32_t pX,
    uint32_t pY,
    DirectionFlags pDirection)
{
    return !NumericGuard<uint32_t>::Overflow(pX, DirectionHelper::HorizontalOffset(pDirection))
        && !NumericGuard<uint32_t>::Overflow(pY, DirectionHelper::VerticalOffset(pDirection));
}

}

const Terrain& TerrainFactory::GetTerrain(
    uint32_t pX,
    uint32_t pY)
{
    const ChunkDescriptor lDescriptor = GetChunkDescriptor(pX, pY);

    auto lChunkIt = mChunks.find(lDescriptor);
    if (lChunkIt == mChunks.cend())
    {
        BuildTerrain(lDescriptor);
        lChunkIt = mChunks.find(lDescriptor);
    }

    switch (lDescriptor.Type)
    {
    case ChunkType::Chunk:
    {
        const uint32_t lOffsetX = pX - lDescriptor.XMin;
        const uint32_t lOffsetY = pY - lDescriptor.YMin;
        const uint32_t lIndex = lOffsetX + kTerrainChunkSideSize * lOffsetY;
        return (*lChunkIt->second)[lIndex];
    }
    case ChunkType::HorizontalBorder:
    {
        const uint32_t lOffsetX = pX - lDescriptor.XMin;
        return (*lChunkIt->second)[lOffsetX];
    }
    case ChunkType::VerticalBorder:
    {
        const uint32_t lOffsetY = pY - lDescriptor.YMin;
        return (*lChunkIt->second)[lOffsetY];
    }
    case ChunkType::Corner:
        return (*lChunkIt->second).front();
    }
}

TerrainFactory::ChunkDescriptor TerrainFactory::GetChunkDescriptor(
    uint32_t pX,
    uint32_t pY) const
{
    const bool lIsHorizontalBorder = ((pY % kTerrainChunkSideStride + 1) % kTerrainChunkSideStride == 0);
    const bool lIsVerticalBorder = ((pX % kTerrainChunkSideStride + 1) % kTerrainChunkSideStride == 0);

    // Origin of the nearest chunk.
    const uint32_t lXMin = pX - pX % kTerrainChunkSideStride;
    const uint32_t lYMin = pY - pY % kTerrainChunkSideStride;

    if (lIsHorizontalBorder && lIsVerticalBorder)
    {
        return ChunkDescriptor{ pX, pY, ChunkType::Corner };
    }
    else if (lIsHorizontalBorder)
    {
        return ChunkDescriptor{ lXMin, pY, ChunkType::HorizontalBorder };
    }
    else if (lIsVerticalBorder)
    {
        return ChunkDescriptor{ pX, lYMin, ChunkType::VerticalBorder };
    }
    else
    {
        return ChunkDescriptor{ lXMin, lYMin, ChunkType::Chunk };
    }
}

void TerrainFactory::BuildTerrain(
    const TerrainFactory::ChunkDescriptor& pDescriptor)
{
    switch (pDescriptor.Type)
    {
    case ChunkType::Chunk:
        BuildTerrainChunk(pDescriptor);
        break;
    case ChunkType::HorizontalBorder:
    case ChunkType::VerticalBorder:
        BuildTerrainBorder(pDescriptor);
        break;
    case ChunkType::Corner:
        BuildTerrainCorner(pDescriptor);
        break;
    }
}

void TerrainFactory::BuildTerrainChunk(
    const TerrainFactory::ChunkDescriptor& pDescriptor)
{
    static std::mt19937_64 sRandomEngine;
    sRandomEngine.seed((static_cast<uint64_t>(pDescriptor.XMin) << 32) | pDescriptor.YMin);

    std::shared_ptr<Chunk> lChunk = mChunks[pDescriptor] = std::make_shared<Chunk>();
    lChunk->assign(kTerrainChunkSideSize * kTerrainChunkSideSize, Terrain{ false, 0 });

    // Build a complete graph.
    constexpr uint32_t lTerrainChunkNodesCount = kTerrainChunkSideNodes * kTerrainChunkSideNodes;
    std::vector<TerrainNode> lNodes(lTerrainChunkNodesCount, { DirectionFlags::None });
    std::vector<TerrainEdge> lEdges;
    for (uint32_t lX = 0; lX < kTerrainChunkSideNodes; lX++)
    {
        for (uint32_t lY = 0; lY < kTerrainChunkSideNodes; lY++)
        {
            const uint32_t lIndex = lX + lY * kTerrainChunkSideNodes;
            if (lY > 0)
            {
                const uint32_t lTargetIndex = lX + (lY - 1) * kTerrainChunkSideNodes;
                lEdges.push_back(TerrainEdge{ lIndex, lTargetIndex, false });
            }
            if (lX > 0)
            {
                const uint32_t lTargetIndex = (lX - 1) + lY * kTerrainChunkSideNodes;
                lEdges.push_back(TerrainEdge{ lIndex, lTargetIndex, false });
            }
        }
    }

    // Build a random spanning tree.
    std::shuffle(lEdges.begin(), lEdges.end(), sRandomEngine);
    DisjointSet lDisjointSet(lTerrainChunkNodesCount);
    for (auto& lEdge : lEdges)
    {
        if (lDisjointSet.Merge(lEdge.U, lEdge.V))
        {
            lEdge.Walkable = true;
            lNodes[lEdge.U].Edges |= TerrainEdge::Direction(lEdge.U, lEdge.V);
            lNodes[lEdge.V].Edges |= TerrainEdge::Direction(lEdge.V, lEdge.U);
        }
    }

    // Remove dead-ends.
    for (auto& lEdge : lEdges)
    {
        if (!lEdge.Walkable)
        {
            if (TerrainNode::IsLeaf(lNodes[lEdge.U].Edges) ||
                TerrainNode::IsLeaf(lNodes[lEdge.V].Edges))
            {
                lEdge.Walkable = true;
                lNodes[lEdge.U].Edges |= TerrainEdge::Direction(lEdge.U, lEdge.V);
                lNodes[lEdge.V].Edges |= TerrainEdge::Direction(lEdge.V, lEdge.U);
            }
        }
    }

    for (uint32_t lNode = 0; lNode < lTerrainChunkNodesCount; lNode++)
    {
        const TerrainNode::Coord lCoord = TerrainNode::IndexToGlobalCoord(lNode);
        const uint32_t lIndex = lCoord.X + kTerrainChunkSideSize * lCoord.Y;
        Terrain& lNodeTerrain = lChunk->at(lIndex);

        // Add flags for paths crossing chunk borders.
        for (auto&& lDirection : Core::DirectionHelper::DirectionFlagsEnumerator{})
        {
            if (!NextNodeIsInBounds(pDescriptor.XMin + lCoord.X, pDescriptor.YMin + lCoord.Y, lDirection))
            {
                continue;
            }
            const uint32_t lTargetX = pDescriptor.XMin + lCoord.X + DirectionHelper::HorizontalOffset(lDirection);
            const uint32_t lTargetY = pDescriptor.YMin + lCoord.Y + DirectionHelper::VerticalOffset(lDirection);
            if (GetChunkDescriptor(lTargetX, lTargetY) != pDescriptor && GetTerrain(lTargetX, lTargetY).Walkable)
            {
                lNodes[lNode].Edges |= lDirection;
            }
        }

        // Nodes are always walkable.
        lNodeTerrain.Walkable = true;
        lNodeTerrain.Index = GetTilesetTextureIndex(lNodes[lNode].Edges);
    }
    for (const auto& lEdge : lEdges)
    {
        if (lEdge.Walkable)
        {
            const TerrainNode::Coord lCoordU = TerrainNode::IndexToLocalCoord(lEdge.U);
            const TerrainNode::Coord lCoordV = TerrainNode::IndexToLocalCoord(lEdge.V);
            const TerrainNode::Coord lCoord = { lCoordU.X + lCoordV.X, lCoordU.Y + lCoordV.Y };
            const uint32_t lIndex = lCoord.X + kTerrainChunkSideSize * lCoord.Y;
            Terrain& lNodeTerrain = lChunk->at(lIndex);

            lNodeTerrain.Walkable = true;
            lNodeTerrain.Index = GetTilesetTextureIndex(
                TerrainEdge::Direction(lEdge.U, lEdge.V) |
                TerrainEdge::Direction(lEdge.V, lEdge.U));
        }
    }
}

void TerrainFactory::BuildTerrainBorder(
    const TerrainFactory::ChunkDescriptor& pDescriptor)
{
    static std::mt19937_64 sRandomEngine;
    sRandomEngine.seed((static_cast<uint64_t>(pDescriptor.XMin) << 32) | pDescriptor.YMin);

    std::shared_ptr<Chunk> lChunk = mChunks[pDescriptor] = std::make_shared<Chunk>();
    lChunk->assign(kTerrainChunkSideSize, Terrain{ false, 0 });

    std::uniform_real_distribution lDistribution(0.0, 1.0);
    for (uint32_t lNode = 0; lNode < kTerrainChunkSideNodes; lNode++)
    {
        Terrain& lNodeTerrain = lChunk->at(TerrainNode::LocalToGlobal(lNode));
        if (lDistribution(sRandomEngine) < 0.5)
        {
            lNodeTerrain.Walkable = true;
            switch (pDescriptor.Type)
            {
            case ChunkType::HorizontalBorder:
                lNodeTerrain.Index = GetTilesetTextureIndex(DirectionFlags::Vertical);
                break;
            case ChunkType::VerticalBorder:
                lNodeTerrain.Index = GetTilesetTextureIndex(DirectionFlags::Horizontal);
                break;
            }

        }
    }
}

void TerrainFactory::BuildTerrainCorner(
    const TerrainFactory::ChunkDescriptor& pDescriptor)
{
    std::shared_ptr<Chunk> lChunk = mChunks[pDescriptor] = std::make_shared<Chunk>();
    lChunk->assign(1, Terrain{ false, 0 });
}

}
