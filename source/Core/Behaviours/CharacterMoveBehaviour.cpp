#include <Core/Behaviours/CharacterMoveBehaviour.hpp>
#include <Core/Enumerator.hpp>

#include <algorithm>
#include <cmath>

namespace Core
{

CharacterMoveCommands::CharacterMoveCommands() :
    mMaxCommandBufferSize(2),
    mCommandBuffer{ Core::Direction::None }
{}

void CharacterMoveCommands::Insert(
    const Core::Direction& pDirection)
{
    if (mCommandBuffer.empty())
    {
        mCommandBuffer.push_back(pDirection);
    }
    else if (mCommandBuffer.back() == Core::DirectionHelper::Opposite(pDirection))
    {
        mCommandBuffer.pop_back();
        if (mCommandBuffer.empty())
        {
            mCommandBuffer.push_back(pDirection);
        }
    }
    else if (mCommandBuffer.back() == Core::Direction::None)
    {
        mCommandBuffer.pop_back();
        Insert(pDirection);
    }
    else if (mCommandBuffer.size() == mMaxCommandBufferSize)
    {
        mCommandBuffer.pop_back();
        Insert(pDirection);
    }
    else if (mCommandBuffer.back() != pDirection)
    {
        mCommandBuffer.push_back(pDirection);
    }
}

void CharacterMoveCommands::Consume(
    size_t pCount)
{
    mCommandBuffer.erase(mCommandBuffer.begin(), mCommandBuffer.begin() + std::min(pCount, mCommandBuffer.size()));
    if (mCommandBuffer.empty())
    {
        mCommandBuffer.push_back(Core::Direction::None);
    }
}

void CharacterMoveCommands::Invalidate(
    size_t pIndex)
{
    mCommandBuffer.erase(mCommandBuffer.begin() + pIndex + 1, mCommandBuffer.end());
}

void CharacterMoveCommands::SetMaxSize(
    size_t pMaxSize)
{
    mMaxCommandBufferSize = std::max(static_cast<size_t>(1), pMaxSize);
    if (mCommandBuffer.size() > mMaxCommandBufferSize)
    {
        mCommandBuffer.resize(mMaxCommandBufferSize);
    }
}

size_t CharacterMoveCommands::Size() const
{
    return mCommandBuffer.size();
}

Core::Direction CharacterMoveCommands::Get(
    size_t pIndex) const
{
    return mCommandBuffer[pIndex];
}

bool CharacterMoveCommands::HasNext(
    size_t pIndex) const
{
    return pIndex + 1 < mCommandBuffer.size();
}

Core::Direction CharacterMoveCommands::GetNext(
    size_t pIndex) const
{
    return mCommandBuffer[pIndex + 1];
}

bool CharacterMoveBehaviour::Update(
    const Core::LocalView& pLocalView,
    Core::WorldCoord& pPosition,
    Core::CharacterMoveCommands& pCommands,
    Core::TerrainFactory& pTerrainFactory,
    float pDistance)
{
    mPathHitsWall = false;
    mPositionOutOfBounds = false;
    mPositionHitsWall = false;

    if (!pLocalView.Contains(pPosition))
    {
        mPositionOutOfBounds = true;
        return false;
    }

    // Invariant: The position is centered on the current direction axis.
    ConstrainPositionOnDirectionAxis(pPosition, pCommands);

    // Invariant: The position is on a walkable cell and does not collide with a wall.
    if (!ConstrainPositionOnWalkableCell(pPosition, pTerrainFactory))
    {
        return false;
    }

    BuildPath(pPosition, pCommands, pTerrainFactory);
    return Advance(pLocalView, pPosition, pCommands, pDistance);
}

void CharacterMoveBehaviour::SetMaxPathLength(
    uint32_t pMaxPathLength)
{
    mMaxPathLength = std::max(static_cast<uint32_t>(1), pMaxPathLength);
}

void CharacterMoveBehaviour::ConstrainPositionOnDirectionAxis(
    Core::WorldCoord& pPosition,
    const Core::CharacterMoveCommands& pCommands)
{
    pPosition.Center(Core::DirectionHelper::Axis(pCommands.Get(0)));
}

bool CharacterMoveBehaviour::ConstrainPositionOnWalkableCell(
    Core::WorldCoord& pPosition,
    Core::TerrainFactory& pTerrainFactory)
{
    // The position must not collide with a wall.
    if (pTerrainFactory.GetTerrain(pPosition.Xi, pPosition.Yi).Walkable)
    {
        for (Core::Direction lDirection : Core::DirectionHelper::DirectionValuesEnumerator{})
        {
            if (HasCrossedCellCenter(pPosition, lDirection) && !CheckNextCell(pPosition, lDirection, pTerrainFactory))
            {
                pPosition.Center();
            }
        }
        return true;
    }

    Core::WorldCoord lClosestWalkablePosition{};

    // Find the closest walkable cell, iteratively increasing the search distance.
    uint32_t lClosestWalkableDistance = std::numeric_limits<uint32_t>::max();
    for (int32_t lDistance = 1; lDistance < std::numeric_limits<int32_t>::max(); lDistance++)
    {
        // List the offsets of the cells at the current search distance.
        std::vector<std::pair<int32_t, int32_t>> lOffsets;
        for (int32_t lIndex = -lDistance; lIndex < lDistance; lIndex++)
        {
            lOffsets.push_back(std::make_pair(lIndex, lDistance));
            lOffsets.push_back(std::make_pair(lIndex + 1, -lDistance));
            lOffsets.push_back(std::make_pair(-lDistance, lIndex));
            lOffsets.push_back(std::make_pair(lDistance, lIndex + 1));
        }

        // Find the closest walkable cell.
        for (const auto& [lOffsetX, lOffsetY] : lOffsets)
        {
            if (!pPosition.TranslatedOverflow(lOffsetX, lOffsetY))
            {
                const Core::WorldCoord lTargetPosition = pPosition.Translated(lOffsetX, lOffsetY).Centered();
                if (pTerrainFactory.GetTerrain(lTargetPosition.Xi, lTargetPosition.Yi).Walkable)
                {
                    const uint32_t lTargetDistance = Core::ManhattanDistance(lTargetPosition, pPosition);
                    if (lTargetDistance < lClosestWalkableDistance)
                    {
                        lClosestWalkableDistance = lTargetDistance;
                        lClosestWalkablePosition = lTargetPosition;
                    }
                }
            }
        }
        if (lClosestWalkableDistance != std::numeric_limits<uint32_t>::max())
        {
            pPosition = lClosestWalkablePosition;
            return true;
        }
    }
    return false;
}

void CharacterMoveBehaviour::BuildPath(
    Core::WorldCoord& pPosition,
    Core::CharacterMoveCommands& pCommands,
    Core::TerrainFactory& pTerrainFactory)
{
    mPath.clear();

    Core::WorldCoord lCurrentCellCenter(pPosition.Centered());
    size_t lCurrentCommandIndex{};

    // Skip the first cell if the position is beyond its center point.
    const Core::Direction lInitialDirection = pCommands.Get(lCurrentCommandIndex);
    if (HasCrossedCellCenter(pPosition, lInitialDirection))
    {
        // Invariant: The position does not collide with a wall.
        assert(BuildNextCell(lCurrentCellCenter, lInitialDirection, pTerrainFactory));
    }

    for (uint32_t lIndex{}; lIndex < mMaxPathLength; lIndex++)
    {
        mPath.push_back({ lCurrentCellCenter, lCurrentCommandIndex, lCurrentCommandIndex });

        // Try to take a turn to consume the next command.
        if (pCommands.HasNext(lCurrentCommandIndex))
        {
            if (BuildNextCell(lCurrentCellCenter, pCommands.GetNext(lCurrentCommandIndex), pTerrainFactory))
            {
                mPath.back().pNextCommandIndex = ++lCurrentCommandIndex;
                continue;
            }
        }

        if (!BuildNextCell(lCurrentCellCenter, pCommands.Get(lCurrentCommandIndex), pTerrainFactory))
        {
            mPathHitsWall = true;
            break;
        }
    }

    // Remove the commands that cannot be consumed.
    pCommands.Invalidate(mPath.back().pNextCommandIndex);
}

bool CharacterMoveBehaviour::BuildNextCell(
    Core::WorldCoord& pCurrentCell,
    Core::Direction pDirection,
    Core::TerrainFactory& pTerrainFactory)
{
    const int32_t lOffsetX = Core::DirectionHelper::HorizontalOffset(pDirection);
    const int32_t lOffsetY = Core::DirectionHelper::VerticalOffset(pDirection);
    if (!pCurrentCell.TranslatedOverflow(lOffsetX, lOffsetY))
    {
        const Core::WorldCoord lNextCellCenter = pCurrentCell.Translated(lOffsetX, lOffsetY);
        if (pTerrainFactory.GetTerrain(lNextCellCenter.Xi, lNextCellCenter.Yi).Walkable)
        {
            pCurrentCell = lNextCellCenter;
            return true;
        }
    }
    return false;
}

bool CharacterMoveBehaviour::CheckNextCell(
    const Core::WorldCoord& pCurrentCell,
    Core::Direction pDirection,
    Core::TerrainFactory& pTerrainFactory)
{
    const int32_t lOffsetX = Core::DirectionHelper::HorizontalOffset(pDirection);
    const int32_t lOffsetY = Core::DirectionHelper::VerticalOffset(pDirection);
    if (!pCurrentCell.TranslatedOverflow(lOffsetX, lOffsetY))
    {
        const Core::WorldCoord lNextCellCenter = pCurrentCell.Translated(lOffsetX, lOffsetY);
        if (pTerrainFactory.GetTerrain(lNextCellCenter.Xi, lNextCellCenter.Yi).Walkable)
        {
            return true;
        }
    }
    return false;
}

bool CharacterMoveBehaviour::HasCrossedCellCenter(
    const Core::WorldCoord& pPosition,
    Core::Direction pDirection)
{
    const Core::WorldCoord lCellCenter(pPosition.Centered());
    switch (pDirection)
    {
    case Core::Direction::North:
        return pPosition.Yf > lCellCenter.Yf;
    case Core::Direction::South:
        return pPosition.Yf < lCellCenter.Yf;
    case Core::Direction::East:
        return pPosition.Xf > lCellCenter.Xf;
    case Core::Direction::West:
        return pPosition.Xf < lCellCenter.Xf;
    default:
        return false;
    }
    return false;
}

bool CharacterMoveBehaviour::Advance(
    const Core::LocalView& pLocalView,
    Core::WorldCoord& pPosition,
    Core::CharacterMoveCommands& pCommands,
    float pDistance)
{
    // Check pre-conditions.
    assert(!mPath.empty());

    const Node& lFirstNode = mPath.front();
    if (!pLocalView.Contains(pPosition) || !pLocalView.Contains(lFirstNode.Center))
    {
        mPositionOutOfBounds = true;
        return false;
    }

    // Move towards the first node.
    const Core::LocalCoordF lCurrentPositionLocal = pLocalView.MapToLocal(pPosition);
    const Core::LocalCoordF lFirstNodeCenterLocal = pLocalView.MapToLocal(lFirstNode.Center);
    const float lDistanceToCenter = Core::ManhattanDistance(lCurrentPositionLocal, lFirstNodeCenterLocal);
    if (pDistance <= lDistanceToCenter)
    {
        pPosition.Translate(
            Core::DirectionHelper::HorizontalOffset(pCommands.Get(lFirstNode.pPrevCommandIndex)) * pDistance,
            Core::DirectionHelper::VerticalOffset(pCommands.Get(lFirstNode.pPrevCommandIndex)) * pDistance);
        return true;
    }
    pDistance -= lDistanceToCenter;
    pPosition = lFirstNode.Center;

    // Move after the last crossed node.
    const size_t lLastNodeIndex = std::floor(pDistance);
    if (lLastNodeIndex < mPath.size() - 1)
    {
        const Node& lLastNode = mPath[lLastNodeIndex];
        pDistance = std::fmod(pDistance, 1.0f);
        pPosition = lLastNode.Center.Translated(
            Core::DirectionHelper::HorizontalOffset(pCommands.Get(lLastNode.pNextCommandIndex)) * pDistance,
            Core::DirectionHelper::VerticalOffset(pCommands.Get(lLastNode.pNextCommandIndex)) * pDistance);
        pCommands.Consume(lLastNode.pNextCommandIndex);
    }
    else
    {
        const Node& lLastNode = mPath.back();
        pPosition = lLastNode.Center;
        pCommands.Consume(lLastNode.pNextCommandIndex);
        mPositionHitsWall = mPathHitsWall;
    }

    mPositionOutOfBounds = !pLocalView.Contains(pPosition);
    return !(mPositionOutOfBounds || mPositionHitsWall);
}

}
