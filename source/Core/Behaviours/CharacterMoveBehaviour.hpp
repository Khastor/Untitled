#ifndef CORE_BEHAVIOURS_CHARACTER_MOVE_BEHAVIOUR_HPP_INCLUDED
#define CORE_BEHAVIOURS_CHARACTER_MOVE_BEHAVIOUR_HPP_INCLUDED

#include <Core/Direction.hpp>
#include <Core/Coordinates.hpp>
#include <Core/TerrainFactory.hpp>

#include <vector>

namespace Core
{

/**
 * @brief Character move command buffer.
 *
 * The buffer always contains at least one move command.
 * The buffer is initialized to Core::Direction::None.
 */
class CharacterMoveCommands
{
public:
    CharacterMoveCommands();

    /**
     * @brief Insert a move command at the end of the buffer.
     *
     * The move command must be one of the 4 cardinal directions.
     * The last move command can be cancelled by inserting the opposite move command.
     * The last move command can be updated if the buffer is full.
     *
     * @param pDirection The move command to insert.
     */
    void Insert(
        const Core::Direction& pDirection);

    /**
     * @brief Consume move commands from the start of the buffer.
     *
     * Consume the first pCount move commands from the buffer.
     * If this would empty the buffer, the buffer is reset to Core::Direction::None.
     *
     * @param pCount Number of move commands to consume.
     */
    void Consume(
        size_t pCount);

    /**
     * @brief Invalidate subsequent move commands.
     *
     * Erase all the move commands after pIndex from the buffer.
     *
     * @param pIndex Index of the last move command.
     */
    void Invalidate(
        size_t pIndex);

    /**
     * @brief Set the maximum size of the buffer.
     *
     * @param pMaxSize The maximum size of the buffer.
     */
    void SetMaxSize(
        size_t pMaxSize);

    /**
     * @brief Get the current size of the buffer.
     *
     * @return The current size of the buffer.
     */
    size_t Size() const;

    /**
     * @brief Get the move command at an index.
     *
     * @param pIndex The index of the move command.
     * @return The move command at pIndex.
     */
    Core::Direction Get(
        size_t pIndex) const;

    /**
     * @brief Check if there is a move command after an index.
     *
     * @param pIndex The index of the previous move command.
     * @return true if there is a move command after pIndex.
     */
    bool HasNext(
        size_t pIndex) const;

    /**
     * @brief Get the move command after an index.
     *
     * @param pIndex The index of the previous move command.
     * @return The move command after pIndex.
     */
    Core::Direction GetNext(
        size_t pIndex) const;

private:
    size_t mMaxCommandBufferSize;
    std::vector<Core::Direction> mCommandBuffer;
};

/**
 * @brief Character move behaviour.
 */
class CharacterMoveBehaviour
{
public:
    /**
     * @brief Update character position and move commands buffer.
     *
     * Move pPosition by pDistance within pLocalView by consuming the move commands in pCommands.
     * The return code is set to false if an event occurred: the position moved out of local view bounds or hit a wall.
     *
     * @param pLocalView The local view bounds.
     * @param pPosition The character position to update.
     * @param pCommands The move commands.
     * @param pTerrainFactory The terrain factory.
     * @param pDistance The distance to travel.
     * @return true if the update succeeded without notable events.
     */
    bool Update(
        const Core::LocalView& pLocalView,
        Core::WorldCoord& pPosition,
        Core::CharacterMoveCommands& pCommands,
        Core::TerrainFactory& pTerrainFactory,
        float pDistance);

    /**
     * @brief Set the maximum path length.
     *
     * @param pMaxPathLength The maximum number of cells in the path computed from the move commands buffer.
     */
    void SetMaxPathLength(
        uint32_t pMaxPathLength);

// TODO: Add private access specifier (not done for debugging purposes).
//private:
    void ConstrainPositionOnDirectionAxis(
        Core::WorldCoord& pPosition,
        const Core::CharacterMoveCommands& pCommands);

    bool ConstrainPositionOnWalkableCell(
        Core::WorldCoord& pPosition,
        Core::TerrainFactory& pTerrainFactory);

    void BuildPath(
        Core::WorldCoord& pPosition,
        Core::CharacterMoveCommands& pCommands,
        Core::TerrainFactory& pTerrainFactory);

    bool BuildNextCell(
        Core::WorldCoord& pCurrentCell,
        Core::Direction pDirection,
        Core::TerrainFactory& pTerrainFactory);

    bool CheckNextCell(
        const Core::WorldCoord& pCurrentCell,
        Core::Direction pDirection,
        Core::TerrainFactory& pTerrainFactory);
    
    bool HasCrossedCellCenter(
        const Core::WorldCoord& pPosition,
        Core::Direction pDirection);

    // Pre-condition: mPath must not be empty.
    bool Advance(
        const Core::LocalView& pLocalView,
        Core::WorldCoord& pPosition,
        Core::CharacterMoveCommands& pCommands,
        float pDistance);

    struct Node
    {
        Core::WorldCoord Center;
        size_t pPrevCommandIndex;
        size_t pNextCommandIndex;
    };

    std::vector<Node> mPath;
    uint32_t mMaxPathLength;

    bool mPathHitsWall;
    bool mPositionOutOfBounds;
    bool mPositionHitsWall;
};

}

#endif // CORE_BEHAVIOURS_CHARACTER_MOVE_BEHAVIOUR_HPP_INCLUDED
