#ifndef CORE_TERRAIN_FACTORY_HPP_INCLUDED
#define CORE_TERRAIN_FACTORY_HPP_INCLUDED

#include <Core/Terrain.hpp>

#include <cstddef>
#include <cstdint>
#include <memory>
#include <unordered_map>
#include <vector>

namespace Core
{

class TerrainFactory
{
public:
    const Terrain& GetTerrain(
        uint32_t pX,
        uint32_t pY);

private:
    using Chunk = std::vector<Terrain>;

    enum class ChunkType
    {
        Chunk, HorizontalBorder, VerticalBorder, Corner
    };

    struct ChunkDescriptor
    {
        uint32_t XMin;
        uint32_t YMin;
        ChunkType Type;

        bool operator==(
            const ChunkDescriptor& pOther) const
        {
            return XMin == pOther.XMin && YMin == pOther.YMin && Type == pOther.Type;
        }

        bool operator!=(
            const ChunkDescriptor& pOther) const
        {
            return XMin != pOther.XMin || YMin != pOther.YMin || Type != pOther.Type;
        }
    };

    struct ChunkDescriptorHash
    {
        size_t operator()(
            const ChunkDescriptor& pDescriptor) const
        {
            return (static_cast<size_t>(pDescriptor.XMin) << 32) | pDescriptor.YMin;
        }
    };

    ChunkDescriptor GetChunkDescriptor(
        uint32_t pX,
        uint32_t pY) const;

    void BuildTerrain(
        const ChunkDescriptor& pDescriptor);

    void BuildTerrainChunk(
        const ChunkDescriptor& pDescriptor);

    void BuildTerrainBorder(
        const ChunkDescriptor& pDescriptor);

    void BuildTerrainCorner(
        const ChunkDescriptor& pDescriptor);

    // TODO: cleanup old chunks that are very far away
    std::unordered_map<ChunkDescriptor, std::shared_ptr<Chunk>, ChunkDescriptorHash> mChunks;
};

}

#endif // CORE_TERRAIN_FACTORY_HPP_INCLUDED
