#include <Core/Algorithms/DisjointSet.hpp>

#include <numeric>

namespace Core
{

DisjointSet::DisjointSet(
    int32_t pCount) :
    mParent(pCount),
    mRank(pCount, 0),
    mSize(pCount, 1)
{
    std::iota(mParent.begin(), mParent.end(), 0);
}

bool DisjointSet::Merge(
    int32_t pIndexU,
    int32_t pIndexV)
{
    const int32_t lDisjointSetIndexU = Find(pIndexU);
    const int32_t lDisjointSetIndexV = Find(pIndexV);
    if (lDisjointSetIndexU == lDisjointSetIndexV)
    {
        return false;
    }

    if (mRank[lDisjointSetIndexU] > mRank[lDisjointSetIndexV])
    {
        mParent[lDisjointSetIndexV] = lDisjointSetIndexU;
        mSize[lDisjointSetIndexU] += mSize[lDisjointSetIndexV];
    }
    else
    {
        mParent[lDisjointSetIndexU] = lDisjointSetIndexV;
        mSize[lDisjointSetIndexV] += mSize[lDisjointSetIndexU];
        if (mRank[lDisjointSetIndexU] == mRank[lDisjointSetIndexV])
        {
            mRank[lDisjointSetIndexV]++;
        }
    }
    return true;
}

bool DisjointSet::Disjoint(
    int32_t pIndexU,
    int32_t pIndexV)
{
    return Find(pIndexU) != Find(pIndexV);
}

int32_t DisjointSet::Size(
    int32_t pIndex)
{
    return mSize[Find(pIndex)];
}

int32_t DisjointSet::Find(
    int32_t pIndex)
{
    return (mParent[pIndex] == pIndex) ? pIndex : (mParent[pIndex] = Find(mParent[pIndex]));
}

}
