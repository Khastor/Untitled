#ifndef CORE_ALGORITHMS_DISJOINT_SET_HPP_INCLUDED
#define CORE_ALGORITHMS_DISJOINT_SET_HPP_INCLUDED

#include <cstdint>
#include <vector>

namespace Core
{

/**
 * @brief Disjoint-Set data structure.
 *
 * The Disjoint-Set data structure is a collection of disjoint sets.
 * It provides operations for merging disjoint sets and testing if sets are disjoint.
 */
class DisjointSet
{
public:
    /**
     * @brief Create a Disjoint-Set data structure.
     *
     * The Disjoint-Set data structure is filled with pCount disjoint sets.
     * The disjoint sets are mapped to indices in the range [0..pCount-1].
     *
     * @param pCount Initial number of disjoint sets.
     */
    DisjointSet(
        int32_t pCount);

    /**
     * @brief Merge two disjoint sets.
     *
     * This operation has no effect if the set indices are mapped to the same disjoint set.
     *
     * The original disjoint sets are merged into a single disjoint set.
     * The original disjoint set indices are remapped to the new disjoint set.
     * Disjoint sets can be addressed by the index of any set they contain.
     *
     * @param pIndexU Index of the first set.
     * @param pIndexV Index of the second set.
     * @return true if two disjoint sets have been merged.
     */
    bool Merge(
        int32_t pIndexU,
        int32_t pIndexV);

    /**
     * @brief Test if two sets are disjoint.
     *
     * Two sets are disjoint if their indices are mapped to a different disjoint set.
     *
     * @param pIndexU Index of the first set.
     * @param pIndexV Index of the second set.
     * @return true if the two sets are disjoint.
     */
    bool Disjoint(
        int32_t pIndexU,
        int32_t pIndexV);

    /**
     * @brief Get the size of the disjoint set.
     *
     * Get the size of the disjoint set mapped to the set index.
     *
     * @param pIndex Index of the disjoint set.
     * @return The size of the disjoint set.
     */
    int32_t Size(
        int32_t pIndex);

private:
    /**
     * @brief Find the disjoint set mapped by a set index.
     *
     * Disjoint sets can be addressed by the index of any set they contain.
     *
     * @param pIndex Index of the set.
     * @return The identifier of the disjoint set.
     */
    int32_t Find(
        int32_t pIndex);

    std::vector<int32_t> mParent;
    std::vector<int32_t> mRank;
    std::vector<int32_t> mSize;
};

}

#endif // CORE_ALGORITHMS_DISJOINT_SET_HPP_INCLUDED
