#include <TextureArray.hpp>

#include <glad/gl.h>

#include <stb/stb_image.h>

TextureArray::TextureArray(
    const std::string& pFilename,
    uint32_t pSubTextureWidth,
    uint32_t pSubTextureHeight,
    uint32_t pCount) :
    mHandle(0)
{
    int lWidth, lHeight, lChannels;
    if (uint8_t* lTexels = stbi_load(pFilename.c_str(), &lWidth, &lHeight, &lChannels, 0); lChannels == 4)
    {
        if ((lWidth % pSubTextureWidth != 0 || lHeight % pSubTextureHeight != 0) ||
            ((lWidth / pSubTextureWidth) * (lHeight / pSubTextureHeight) < pCount))
        {
            stbi_image_free(lTexels);
            return;
        }

        glCreateTextures(GL_TEXTURE_2D_ARRAY, 1, &mHandle);
        glTextureStorage3D(mHandle, 1, GL_RGBA8, pSubTextureWidth, pSubTextureHeight, pCount);

        const uint32_t lSubTextureCountX = lWidth / pSubTextureWidth;
        const uint32_t lSubTextureCountY = lHeight / pSubTextureHeight;
        for (uint32_t lSubTextureIndex = 0; lSubTextureIndex < pCount; lSubTextureIndex++)
        {
            const uint32_t lSubTextureX = lSubTextureIndex % lSubTextureCountX;
            const uint32_t lSubTextureY = lSubTextureIndex / lSubTextureCountX;
            uint32_t lOffset = 4 * (lSubTextureY * lSubTextureCountX * pSubTextureWidth * pSubTextureHeight + pSubTextureWidth * lSubTextureX);
            for (uint32_t lLineIndex = 0; lLineIndex < pSubTextureHeight; lLineIndex++)
            {
                glTextureSubImage3D(mHandle, 0, 0, lLineIndex, lSubTextureIndex, pSubTextureWidth, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, lTexels + lOffset);
                lOffset += 4 * lSubTextureCountX * pSubTextureWidth;
            }
        }

        glTextureParameteri(mHandle, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTextureParameteri(mHandle, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTextureParameteri(mHandle, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTextureParameteri(mHandle, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        stbi_image_free(lTexels);
    }
}

TextureArray::~TextureArray()
{
    glDeleteTextures(1, &mHandle);
}

std::shared_ptr<TextureArray> TextureArray::Create(
    const std::string& pFilename,
    uint32_t pSubTextureWidth,
    uint32_t pSubTextureHeight,
    uint32_t pCount)
{
    return std::make_shared<TextureArray>(pFilename, pSubTextureWidth, pSubTextureHeight, pCount);
}

bool TextureArray::IsNull() const
{
    return mHandle == 0;
}

void TextureArray::Bind(
    uint32_t pUnit) const
{
    glBindTextureUnit(pUnit, mHandle);
}
