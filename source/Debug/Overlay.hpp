#ifndef DEBUG_OVERLAY_HPP_INCLUDED
#define DEBUG_OVERLAY_HPP_INCLUDED

#include <Core/Camera.hpp>
#include <Core/Coordinates.hpp>
#include <Shader.hpp>

#include <glm/glm.hpp>

#include <glad/gl.h>

#include <memory>
#include <utility>
#include <vector>

namespace Debug
{

class Overlay
{
public:
    Overlay();

    ~Overlay();

    void Clear();

    void Add(
        const Core::WorldCoord& pCoord,
        const glm::vec4& pColor);

    void Add(
        const Core::WorldFrame& pFrame,
        const glm::vec4& pColor);

    void Draw(
        const Core::Camera& pCamera);

private:
    std::shared_ptr<Shader> mShader;
    std::vector<std::pair<Core::WorldCoord, glm::vec4>> mCoords;
    std::vector<std::pair<Core::WorldFrame, glm::vec4>> mFrames;
    GLuint mObjectHandle;
    GLuint mBufferHandle;
    GLsizeiptr mBufferSize;
};

}

#endif // DEBUG_OVERLAY_HPP_INCLUDED
