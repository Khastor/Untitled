#include <Debug/Overlay.hpp>
#include <Shader.hpp>

#include <glad/gl.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <memory>
#include <utility>
#include <vector>

namespace Debug
{

namespace
{

void AddVertex(
    std::vector<GLfloat>& pVertexBuffer,
    float pCoordX,
    float pCoordY,
    const glm::vec4& pColor)
{
    pVertexBuffer.push_back(pCoordX);
    pVertexBuffer.push_back(pCoordY);
    pVertexBuffer.push_back(pColor.r);
    pVertexBuffer.push_back(pColor.g);
    pVertexBuffer.push_back(pColor.b);
    pVertexBuffer.push_back(pColor.a);
}

}

Overlay::Overlay() :
    mObjectHandle(0),
    mBufferHandle(0),
    mBufferSize(0)
{
    mShader = std::make_shared<Shader>("assets/shaders/Debug");

    glCreateVertexArrays(1, &mObjectHandle);
    glCreateBuffers(1, &mBufferHandle);

    glVertexArrayVertexBuffer(mObjectHandle, 0, mBufferHandle, 0, 6 * sizeof(GLfloat));

    glEnableVertexArrayAttrib(mObjectHandle, 0);
    glVertexArrayAttribFormat(mObjectHandle, 0, 2, GL_FLOAT, GL_FALSE, 0);
    glVertexArrayAttribBinding(mObjectHandle, 0, 0);

    glEnableVertexArrayAttrib(mObjectHandle, 1);
    glVertexArrayAttribFormat(mObjectHandle, 1, 4, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat));
    glVertexArrayAttribBinding(mObjectHandle, 1, 0);
}

Overlay::~Overlay()
{
    glDeleteVertexArrays(1, &mObjectHandle);
    glDeleteBuffers(1, &mBufferHandle);
}


void Overlay::Clear()
{
    mCoords.clear();
    mFrames.clear();
}

void Overlay::Add(
    const Core::WorldCoord& pCoord,
    const glm::vec4& pColor)
{
    mCoords.push_back(std::make_pair(pCoord, pColor));
}

void Overlay::Add(
    const Core::WorldFrame& pFrame,
    const glm::vec4& pColor)
{
    mFrames.push_back(std::make_pair(pFrame, pColor));
}

void Overlay::Draw(
    const Core::Camera& pCamera)
{
    const Core::LocalView lCameraView = pCamera.GetLocalView();

    std::vector<GLfloat> lVertexBuffer;
    for (auto& [lWorldCoord, lColor] : mCoords)
    {
        if (!lCameraView.Contains(lWorldCoord))
        {
            continue;
        }
        const Core::LocalCoordF lLocalCoord = lCameraView.MapToLocal(lWorldCoord);

        // Add coord lines (Horizontal, Vertical).
        AddVertex(lVertexBuffer, lLocalCoord.X - 0.5f, lLocalCoord.Y, lColor);
        AddVertex(lVertexBuffer, lLocalCoord.X + 0.5f, lLocalCoord.Y, lColor);
        AddVertex(lVertexBuffer, lLocalCoord.X, lLocalCoord.Y - 0.5f, lColor);
        AddVertex(lVertexBuffer, lLocalCoord.X, lLocalCoord.Y + 0.5f, lColor);
    }
    for (auto& [lWorldFrame, lColor] : mFrames)
    {
        if (!lCameraView.Contains(lWorldFrame))
        {
            continue;
        }
        const Core::LocalCoordF lMinLocalCoord = lCameraView.MapToLocal(lWorldFrame.Min);
        const Core::LocalCoordF lMaxLocalCoord = lCameraView.MapToLocal(lWorldFrame.Max);

        // Add frame lines (Top, Bottom, Right, Left).
        AddVertex(lVertexBuffer, lMinLocalCoord.X, lMaxLocalCoord.Y, lColor);
        AddVertex(lVertexBuffer, lMaxLocalCoord.X, lMaxLocalCoord.Y, lColor);
        AddVertex(lVertexBuffer, lMinLocalCoord.X, lMinLocalCoord.Y, lColor);
        AddVertex(lVertexBuffer, lMaxLocalCoord.X, lMinLocalCoord.Y, lColor);
        AddVertex(lVertexBuffer, lMaxLocalCoord.X, lMinLocalCoord.Y, lColor);
        AddVertex(lVertexBuffer, lMaxLocalCoord.X, lMaxLocalCoord.Y, lColor);
        AddVertex(lVertexBuffer, lMinLocalCoord.X, lMinLocalCoord.Y, lColor);
        AddVertex(lVertexBuffer, lMinLocalCoord.X, lMaxLocalCoord.Y, lColor);
    }

    const Core::LocalFrameF lCameraLocalFrame = lCameraView.MapToLocal(pCamera.GetFrame());
    mShader->Use();
    mShader->SetProjection(glm::ortho(lCameraLocalFrame.Min.X, lCameraLocalFrame.Max.X, lCameraLocalFrame.Min.Y, lCameraLocalFrame.Max.Y));
    mShader->SetTransform(glm::identity<glm::mat4>());

    GLsizeiptr lVertexBufferSize = lVertexBuffer.size() * sizeof(GLfloat);
    if (lVertexBufferSize > mBufferSize)
    {
        mBufferSize = lVertexBufferSize;
        glNamedBufferData(mBufferHandle, lVertexBufferSize, lVertexBuffer.data(), GL_STREAM_DRAW);
    }
    else
    {
        glNamedBufferSubData(mBufferHandle, 0, lVertexBufferSize, lVertexBuffer.data());
    }
    glBindVertexArray(mObjectHandle);
    glDrawArrays(GL_LINES, 0, lVertexBuffer.size() / 6);
    glBindVertexArray(0);
}

}
