#ifndef WINDOW_HPP_INCLUDED
#define WINDOW_HPP_INCLUDED

#include <cstdint>
#include <string>

struct GLFWwindow;

class Window
{
public:
    /**
     * @brief Create the window.
     *
     * The OpenGL context will not be created until the window is shown on the screen.
     * Do not use OpenGL functions in the constructor of the derived class.
     *
     * @param pOpenGLVersionMajor OpenGL major version number.
     * @param pOpenGLVersionMinor OpenGL minor version number.
     * @param pFramebufferSamples Number of MSAA samples.
     * @param pTitle Window title.
     */
    Window(
        uint8_t pOpenGLVersionMajor,
        uint8_t pOpenGLVersionMinor,
        uint8_t pFramebufferSamples,
        std::string pTitle);

    virtual ~Window() = default;

    Window(const Window&) = delete;
    Window& operator=(const Window&) = delete;

    Window(Window&&) = delete;
    Window& operator=(Window&&) = delete;

    /**
     * @brief Show the window on the screen.
     *
     * @param pWidth The initial window width.
     * @param pHeight The initial window height.
     * @param pEnableDebugOutput Enable debug output.
     */
    void Show(
        int32_t pWidth,
        int32_t pHeight);

protected:
    enum class KeyboardKey : int32_t;
    enum class KeyboardMod : int32_t;
    enum class MouseButton : int32_t;

    /**
     * @brief Bitmask type for keyboard modifier codes.
     */
    using KeyboardMods = int32_t;

    /**
     * @brief Create OpenGL resources.
     *
     * This method is called after the initialization of the OpenGL context.
     *
     * @return true if the creation of OpenGL resources was successfull.
     */
    virtual bool OnCreate() = 0;

    /**
     * @brief Update and render the current frame.
     *
     * @param pCurrentTime The current application time in seconds.
     * @param pElapsedTime The time since last frame in seconds.
     *
     * @return false if an error occurred and the window should close.
     */
    virtual bool OnUpdate(
        double pCurrentTime,
        double pElapsedTime) = 0;

    /**
     * @brief Destroy OpenGL resources.
     *
     * This method is called before the destruction of the OpenGL context.
     */
    virtual void OnDestroy() = 0;

    /**
     * @brief Framebuffer resized event handler.
     *
     * The default implementation updates the OpenGL viewport.
     *
     * @param pWidth Framebuffer width in pixels.
     * @param pHeight pFramebuffer height in pixels.
     */
    virtual void OnFramebufferResized(
        int pWidth,
        int pHeight);

    /**
     * @brief Keyboard key pressed event handler.
     *
     * The default implementation is empty.
     *
     * @param pKey The key code.
     * @param pMods The keyboard modifier codes bitmask.
     */
    virtual void OnKeyPressed(
        Window::KeyboardKey pKey,
        Window::KeyboardMods pMods);

    /**
     * @brief Keyboard key released event handler.
     *
     * The default implementation is empty.
     *
     * @param pKey The key code.
     * @param pMods The keyboard modifier codes bitmask.
     */
    virtual void OnKeyReleased(
        Window::KeyboardKey pKey,
        Window::KeyboardMods pMods);

    /**
     * @brief Keyboard key repeated event handler.
     *
     * The default implementation is empty.
     *
     * @param pKey The key code.
     * @param pMods The keyboard modifier codes bitmask.
     */
    virtual void OnKeyRepeated(
        Window::KeyboardKey pKey,
        Window::KeyboardMods pMods);

    /**
     * @brief Mouse button pressed event handler.
     *
     * The default implementation is empty.
     *
     * @param pButton The mouse button code.
     * @param pMods The keyboard modifier codes bitmask.
     */
    virtual void OnMouseButtonPressed(
        Window::MouseButton pButton,
        Window::KeyboardMods pMods);

    /**
     * @brief Mouse button released event handler.
     *
     * The default implementation is empty.
     *
     * @param pButton The mouse button code.
     * @param pMods The keyboard modifier codes bitmask.
     */
    virtual void OnMouseButtonReleased(
        Window::MouseButton pButton,
        Window::KeyboardMods pMods);

    /**
     * @brief Mouse cursor moved event handler.
     *
     * The default implementation is empty.
     *
     * @param pCursorX The mouse cursor position x.
     * @param pCursorY The mouse cursor position y.
     */
    virtual void OnMouseMoved(
        double pCursorX,
        double pCursorY);

    /**
     * @brief Mouse scrolled event handler.
     *
     * The default implementation is empty.
     *
     * @param pOffsetX The mouse scroll offset x.
     * @param pOffsetY The mouse scroll offset y.
     */
    virtual void OnMouseScrolled(
        double pOffsetX,
        double pOffsetY);

    /**
     * @brief File path drop event handler.
     *
     * The default implementation is empty.
     *
     * @param pPaths The array of file paths encoded in UTF-8.
     * @param pCount The size of the array of file paths.
     */
    virtual void OnPathsDropped(
        const char** pPaths,
        uint32_t pCount);

    bool IsIconified() const;

    bool IsFullScreen() const;

    void SetFullScreen(
        bool pFullscreen);

    bool IsCursorEnabled() const;

    void SetCursorEnabled(
        bool pEnabled);

    bool IsFramerateFixed() const;

    void SetFramerateFixed(
        bool pFixed);

    int GetFramebufferWidth() const;

    int GetFramebufferHeight() const;

    double GetCursorX() const;

    double GetCursorY() const;

    bool IsKeyPressed(
        Window::KeyboardKey pKey) const;

    bool IsMouseButtonPressed(
        Window::MouseButton pButton) const;

    /**
     * @brief Keyboard key codes.
     */
    enum class KeyboardKey : int32_t
    {
        KEY_UNKNOWN,
        KEY_SPACE,
        KEY_APOSTROPHE,
        KEY_COMMA,
        KEY_MINUS,
        KEY_PERIOD,
        KEY_SLASH,
        KEY_0,
        KEY_1,
        KEY_2,
        KEY_3,
        KEY_4,
        KEY_5,
        KEY_6,
        KEY_7,
        KEY_8,
        KEY_9,
        KEY_SEMICOLON,
        KEY_EQUAL,
        KEY_A,
        KEY_B,
        KEY_C,
        KEY_D,
        KEY_E,
        KEY_F,
        KEY_G,
        KEY_H,
        KEY_I,
        KEY_J,
        KEY_K,
        KEY_L,
        KEY_M,
        KEY_N,
        KEY_O,
        KEY_P,
        KEY_Q,
        KEY_R,
        KEY_S,
        KEY_T,
        KEY_U,
        KEY_V,
        KEY_W,
        KEY_X,
        KEY_Y,
        KEY_Z,
        KEY_LEFT_BRACKET,
        KEY_BACKSLASH,
        KEY_RIGHT_BRACKET,
        KEY_GRAVE_ACCENT,
        KEY_WORLD_1,
        KEY_WORLD_2,
        KEY_ESCAPE,
        KEY_ENTER,
        KEY_TAB,
        KEY_BACKSPACE,
        KEY_INSERT,
        KEY_DELETE,
        KEY_RIGHT,
        KEY_LEFT,
        KEY_DOWN,
        KEY_UP,
        KEY_PAGE_UP,
        KEY_PAGE_DOWN,
        KEY_HOME,
        KEY_END,
        KEY_CAPS_LOCK,
        KEY_SCROLL_LOCK,
        KEY_NUM_LOCK,
        KEY_PRINT_SCREEN,
        KEY_PAUSE,
        KEY_F1,
        KEY_F2,
        KEY_F3,
        KEY_F4,
        KEY_F5,
        KEY_F6,
        KEY_F7,
        KEY_F8,
        KEY_F9,
        KEY_F10,
        KEY_F11,
        KEY_F12,
        KEY_F13,
        KEY_F14,
        KEY_F15,
        KEY_F16,
        KEY_F17,
        KEY_F18,
        KEY_F19,
        KEY_F20,
        KEY_F21,
        KEY_F22,
        KEY_F23,
        KEY_F24,
        KEY_F25,
        KEY_KP_0,
        KEY_KP_1,
        KEY_KP_2,
        KEY_KP_3,
        KEY_KP_4,
        KEY_KP_5,
        KEY_KP_6,
        KEY_KP_7,
        KEY_KP_8,
        KEY_KP_9,
        KEY_KP_DECIMAL,
        KEY_KP_DIVIDE,
        KEY_KP_MULTIPLY,
        KEY_KP_SUBTRACT,
        KEY_KP_ADD,
        KEY_KP_ENTER,
        KEY_KP_EQUAL,
        KEY_LEFT_SHIFT,
        KEY_LEFT_CONTROL,
        KEY_LEFT_ALT,
        KEY_LEFT_SUPER,
        KEY_RIGHT_SHIFT,
        KEY_RIGHT_CONTROL,
        KEY_RIGHT_ALT,
        KEY_RIGHT_SUPER,
        KEY_MENU,
        KEY_LAST = KEY_MENU
    };

    /**
     * @brief Keyboard modifier codes.
     */
    enum class KeyboardMod : int32_t
    {
        MOD_SHIFT       = 1 << 0,
        MOD_CONTROL     = 1 << 1,
        MOD_ALT         = 1 << 2,
        MOD_SUPER       = 1 << 3,
        MOD_CAPS_LOCK   = 1 << 4,
        MOD_NUM_LOCK    = 1 << 5
    };

    /**
     * @brief Mouse button codes.
     */
    enum class MouseButton : int32_t
    {
        BUTTON_1,
        BUTTON_2,
        BUTTON_3,
        BUTTON_4,
        BUTTON_5,
        BUTTON_6,
        BUTTON_7,
        BUTTON_8,
        BUTTON_LEFT = BUTTON_1,
        BUTTON_RIGHT = BUTTON_2,
        BUTTON_MIDDLE = BUTTON_3
    };

    /**
     * @brief Test if a keyboard modifier is part of the keyboard modifiers bitmask.
     *
     * @param pMods The keyboard modifier bitmask.
     * @param pMod The keyboard modifier to test.
     * @return true if pMod is part of pMods.
     */
    static bool HasKeyboardMod(
        Window::KeyboardMods pMods,
        Window::KeyboardMod pMod);

private:
    static Window::KeyboardKey GLFWCodeToKeyboardKey(
        int32_t pCode);

    static Window::KeyboardMods GLFWCodeToKeyboardMods(
        int32_t pCode);

    static Window::MouseButton GLFWCodeToMouseButton(
        int32_t pCode);

    static int32_t KeyboardKeyToGLFWCode(
        Window::KeyboardKey pCode);

    static int32_t MouseButtonToGLFWCode(
        Window::MouseButton pCode);

    const uint8_t mOpenGLVersionMajor;
    const uint8_t mOpenGLVersionMinor;
    const uint8_t mFramebufferSamples;
    const std::string mTitle; 

    GLFWwindow* mWindow;
    bool mWindowFullscreen;
    bool mWindowIconified;
    bool mWindowFramerateFixed;
    int mWindowFramebufferWidth;
    int mWindowFramebufferHeight;
    int mWindowRestoreWidth;
    int mWindowRestoreHeight;
    int mWindowRestoreX;
    int mWindowRestoreY;
    double mWindowCursorX;
    double mWindowCursorY;
};

#endif // WINDOW_HPP_INCLUDED
