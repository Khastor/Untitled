#ifndef INPUT_STATE_HPP_INCLUDED
#define INPUT_STATE_HPP_INCLUDED

struct InputState
{
    bool MoveLeft = false;
    bool MoveRight = false;
    bool MoveUp = false;
    bool MoveDown = false;
};

#endif // INPUT_STATE_HPP_INCLUDED