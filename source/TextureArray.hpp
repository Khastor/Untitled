#ifndef TEXTURE_ARRAY_HPP_INCLUDED
#define TEXTURE_ARRAY_HPP_INCLUDED

#include <cstdint>
#include <memory>
#include <string>

class TextureArray
{
public:
    TextureArray(
        const std::string& pFilename,
        uint32_t pSubTextureWidth,
        uint32_t pSubTextureHeight,
        uint32_t pCount);

    ~TextureArray();

    TextureArray(const TextureArray&) = delete;
    TextureArray& operator=(const TextureArray&) = delete;

    TextureArray(TextureArray&&) = delete;
    TextureArray& operator=(TextureArray&&) = delete;

    static std::shared_ptr<TextureArray> Create(
        const std::string& pFilename,
        uint32_t pSubTextureWidth,
        uint32_t pSubTextureHeight,
        uint32_t pCount);

    bool IsNull() const;

    void Bind(
        uint32_t pUnit) const;

private:
    uint32_t mHandle;
};

#endif // TEXTURE_ARRAY_HPP_INCLUDED
