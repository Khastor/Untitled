#include "Window.hpp"

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

#include <glad/gl.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <utility>

void GLAPIENTRY GLMessageCallback(
    GLenum pSource,
    GLenum pType,
    GLuint pId,
    GLenum pSeverity,
    GLsizei pLength,
    const GLchar* pMessage,
    const void* pUser)
{
    std::cerr << "[OpenGL] " << pMessage << std::endl;
}

void GLFWMessageCallback(
    int pCode,
    const char* pMessage)
{
    std::cerr << "[GLFW] " << pMessage << std::endl;
}

Window::Window(
    uint8_t pOpenGLVersionMajor,
    uint8_t pOpenGLVersionMinor,
    uint8_t pFramebufferSamples,
    std::string pTitle) :
    mOpenGLVersionMajor(pOpenGLVersionMajor),
    mOpenGLVersionMinor(pOpenGLVersionMinor),
    mFramebufferSamples(pFramebufferSamples),
    mTitle(std::move(pTitle)),
    mWindow(nullptr),
    mWindowFullscreen(false),
    mWindowIconified(false),
    mWindowFramerateFixed(false),
    mWindowFramebufferWidth(0),
    mWindowFramebufferHeight(0),
    mWindowRestoreWidth(0),
    mWindowRestoreHeight(0),
    mWindowRestoreX(0),
    mWindowRestoreY(0),
    mWindowCursorX(0.0),
    mWindowCursorY(0.0)
{}

void Window::Show(
    int32_t pWidth,
    int32_t pHeight)
{
    glfwSetErrorCallback(GLFWMessageCallback);
    if (!glfwInit())
    {
        std::cerr << "[GLFW] Failed to initialize." << std::endl;
        return;
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, mOpenGLVersionMajor);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, mOpenGLVersionMinor);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);
    glfwWindowHint(GLFW_SAMPLES, mFramebufferSamples);

    mWindow = glfwCreateWindow(pWidth, pHeight, mTitle.c_str(), nullptr, nullptr);
    if (!mWindow)
    {
        std::cerr << "[GLFW] Failed to create window." << std::endl;
        glfwTerminate();
        return;
    }

    glfwMakeContextCurrent(mWindow);
    if (!gladLoadGL((GLADloadfunc)glfwGetProcAddress))
    {
        std::cerr << "[GLAD] Failed to load OpenGL." << std::endl;
        glfwTerminate();
        return;
    }

    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(GLMessageCallback, nullptr);

    mWindowIconified = false;
    mWindowFullscreen = false;
    SetFramerateFixed(true);
    glfwGetFramebufferSize(mWindow, &mWindowFramebufferWidth, &mWindowFramebufferHeight);
    glfwGetWindowSize(mWindow, &mWindowRestoreWidth, &mWindowRestoreHeight);
    glfwGetWindowPos(mWindow, &mWindowRestoreX, &mWindowRestoreY);
    glfwGetCursorPos(mWindow, &mWindowCursorX, &mWindowCursorY);
    glfwSetInputMode(mWindow, GLFW_LOCK_KEY_MODS, GLFW_TRUE);
    glViewport(0, 0, mWindowFramebufferWidth, mWindowFramebufferHeight);

    glfwSetWindowUserPointer(mWindow, this);

    glfwSetFramebufferSizeCallback(mWindow,
        [](GLFWwindow* pWindow, int pWidth, int pHeight)
        {
            Window* lWindow = static_cast<Window*>(glfwGetWindowUserPointer(pWindow));
            lWindow->mWindowFramebufferWidth = pWidth;
            lWindow->mWindowFramebufferHeight = pHeight;
            lWindow->OnFramebufferResized(pWidth, pHeight);
        });

    glfwSetWindowRefreshCallback(mWindow,
        [](GLFWwindow* pWindow)
        {
            Window* lWindow = static_cast<Window*>(glfwGetWindowUserPointer(pWindow));
            if (!lWindow->OnUpdate(glfwGetTime(), 0.0))
            {
                glfwSetWindowShouldClose(pWindow, GLFW_TRUE);
            }
        });

    glfwSetWindowIconifyCallback(mWindow,
        [](GLFWwindow* pWindow, int pIconified)
        {
            Window* lWindow = static_cast<Window*>(glfwGetWindowUserPointer(pWindow));
            lWindow->mWindowIconified = pIconified;
        });

    glfwSetCursorPosCallback(mWindow,
        [](GLFWwindow* pWindow, double pCursorX, double pCursorY)
        {
            if (!ImGui::GetIO().WantCaptureMouse)
            {
                Window* lWindow = static_cast<Window*>(glfwGetWindowUserPointer(pWindow));
                lWindow->OnMouseMoved(pCursorX, pCursorY);
            }
        });

    glfwSetScrollCallback(mWindow,
        [](GLFWwindow* pWindow, double pOffsetX, double pOffsetY)
        {
            if (!ImGui::GetIO().WantCaptureMouse)
            {
                Window* lWindow = static_cast<Window*>(glfwGetWindowUserPointer(pWindow));
                lWindow->OnMouseScrolled(pOffsetX, pOffsetY);
            }
        });

    glfwSetMouseButtonCallback(mWindow,
        [](GLFWwindow* pWindow, int pButton, int pAction, int pMods)
        {
            if (!ImGui::GetIO().WantCaptureMouse)
            {
                Window* lWindow = static_cast<Window*>(glfwGetWindowUserPointer(pWindow));
                switch (pAction)
                {
                case GLFW_PRESS:
                    lWindow->OnMouseButtonPressed(GLFWCodeToMouseButton(pButton), GLFWCodeToKeyboardMods(pMods));
                    break;
                case GLFW_RELEASE:
                    lWindow->OnMouseButtonReleased(GLFWCodeToMouseButton(pButton), GLFWCodeToKeyboardMods(pMods));
                    break;
                }
            }
        });

    glfwSetKeyCallback(mWindow,
        [](GLFWwindow* pWindow, int pKey, int pScancode, int pAction, int pMods)
        {
            if (!ImGui::GetIO().WantCaptureKeyboard)
            {
                Window* lWindow = static_cast<Window*>(glfwGetWindowUserPointer(pWindow));
                switch (pAction)
                {
                case GLFW_PRESS:
                    lWindow->OnKeyPressed(GLFWCodeToKeyboardKey(pKey), GLFWCodeToKeyboardMods(pMods));
                    break;
                case GLFW_REPEAT:
                    lWindow->OnKeyRepeated(GLFWCodeToKeyboardKey(pKey), GLFWCodeToKeyboardMods(pMods));
                    break;
                case GLFW_RELEASE:
                    lWindow->OnKeyReleased(GLFWCodeToKeyboardKey(pKey), GLFWCodeToKeyboardMods(pMods));
                    break;
                }
            }
        });

    ImGui::CreateContext();
    ImGui::StyleColorsDark();
    ImGui_ImplGlfw_InitForOpenGL(mWindow, true);
    ImGui_ImplOpenGL3_Init();

    if (OnCreate())
    {
        double lLastTime = glfwGetTime();
        while (!glfwWindowShouldClose(mWindow))
        {
            double lCurrentTime = glfwGetTime();
            double lElapsedTime = lCurrentTime - lLastTime;
            lLastTime = lCurrentTime;

            ImGui_ImplOpenGL3_NewFrame();
            ImGui_ImplGlfw_NewFrame();
            ImGui::NewFrame();

            glfwPollEvents();

            if (!OnUpdate(lCurrentTime, lElapsedTime))
            {
                glfwSetWindowShouldClose(mWindow, GLFW_TRUE);
            }

            ImGui::Render();
            ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

            glfwSwapBuffers(mWindow);
        }
    }

    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    OnDestroy();
    glfwTerminate();
}

void Window::OnFramebufferResized(
    int pWidth,
    int pHeight)
{
    glViewport(0, 0, pWidth, pHeight);
}

void Window::OnKeyPressed(
    Window::KeyboardKey pKey,
    Window::KeyboardMods pMods)
{}

void Window::OnKeyReleased(
    Window::KeyboardKey pKey,
    Window::KeyboardMods pMods)
{}

void Window::OnKeyRepeated(
    Window::KeyboardKey pKey,
    Window::KeyboardMods pMods)
{}

void Window::OnMouseButtonPressed(
    Window::MouseButton pButton,
    Window::KeyboardMods pMods)
{}

void Window::OnMouseButtonReleased(
    Window::MouseButton pButton,
    Window::KeyboardMods pMods)
{}

void Window::OnMouseMoved(
    double pCursorX,
    double pCursorY)
{}

void Window::OnMouseScrolled(
    double pOffsetX,
    double pOffsetY)
{}

void Window::OnPathsDropped(
    const char** pPaths,
    uint32_t pCount)
{}

bool Window::IsIconified() const
{
    return mWindowIconified;
}

bool Window::IsFullScreen() const
{
    return mWindowFullscreen;
}

void Window::SetFullScreen(
    bool pFullscreen)
{
    if (mWindowFullscreen == pFullscreen)
    {
        return;
    }
    if (mWindowFullscreen = pFullscreen)
    {
        GLFWmonitor* lMonitor = glfwGetPrimaryMonitor();
        const GLFWvidmode* lMode = glfwGetVideoMode(lMonitor);

        glfwGetWindowSize(mWindow, &mWindowRestoreWidth, &mWindowRestoreHeight);
        glfwGetWindowPos(mWindow, &mWindowRestoreX, &mWindowRestoreY);
        glfwSetWindowMonitor(mWindow, lMonitor, 0, 0, lMode->width, lMode->height, lMode->refreshRate);
    }
    else
    {
        glfwSetWindowMonitor(mWindow, nullptr, mWindowRestoreX, mWindowRestoreY, mWindowRestoreWidth, mWindowRestoreHeight, 0);
    }
    SetFramerateFixed(IsFramerateFixed());
}

bool Window::IsCursorEnabled() const
{
    return glfwGetInputMode(mWindow, GLFW_CURSOR) == GLFW_CURSOR_NORMAL;
}

void Window::SetCursorEnabled(
    bool pEnabled)
{
    glfwSetInputMode(mWindow, GLFW_CURSOR, pEnabled ? GLFW_CURSOR_NORMAL : GLFW_CURSOR_DISABLED);
}

bool Window::IsFramerateFixed() const
{
    return mWindowFramerateFixed;
}

void Window::SetFramerateFixed(
    bool pFixed)
{
    glfwSwapInterval((mWindowFramerateFixed = pFixed) ? 1 : 0);
}

int Window::GetFramebufferWidth() const
{
    return mWindowFramebufferWidth;
}

int Window::GetFramebufferHeight() const
{
    return mWindowFramebufferHeight;
}

double Window::GetCursorX() const
{
    double lCursorX, lCursorY;
    glfwGetCursorPos(mWindow, &lCursorX, &lCursorY);
    return lCursorX;
}

double Window::GetCursorY() const
{
    double lCursorX, lCursorY;
    glfwGetCursorPos(mWindow, &lCursorX, &lCursorY);
    return lCursorY;
}

bool Window::IsKeyPressed(
    Window::KeyboardKey pKey) const
{
    int lState = glfwGetKey(mWindow, KeyboardKeyToGLFWCode(pKey));
    return lState == GLFW_PRESS || lState == GLFW_REPEAT;
}

bool Window::IsMouseButtonPressed(
    Window::MouseButton pButton) const
{
    int lState = glfwGetMouseButton(mWindow, MouseButtonToGLFWCode(pButton));
    return lState == GLFW_PRESS;
}

bool Window::HasKeyboardMod(
    Window::KeyboardMods pMods,
    Window::KeyboardMod pMod)
{
    return pMods & static_cast<int32_t>(pMod);
}

Window::KeyboardKey Window::GLFWCodeToKeyboardKey(
    int32_t pCode)
{
    switch (pCode)
    {
    case GLFW_KEY_UNKNOWN:
        return Window::KeyboardKey::KEY_UNKNOWN;
    case GLFW_KEY_SPACE:
        return Window::KeyboardKey::KEY_SPACE;
    case GLFW_KEY_APOSTROPHE:
        return Window::KeyboardKey::KEY_APOSTROPHE;
    case GLFW_KEY_COMMA:
        return Window::KeyboardKey::KEY_COMMA;
    case GLFW_KEY_MINUS:
        return Window::KeyboardKey::KEY_MINUS;
    case GLFW_KEY_PERIOD:
        return Window::KeyboardKey::KEY_PERIOD;
    case GLFW_KEY_SLASH:
        return Window::KeyboardKey::KEY_SLASH;
    case GLFW_KEY_0:
        return Window::KeyboardKey::KEY_0;
    case GLFW_KEY_1:
        return Window::KeyboardKey::KEY_1;
    case GLFW_KEY_2:
        return Window::KeyboardKey::KEY_2;
    case GLFW_KEY_3:
        return Window::KeyboardKey::KEY_3;
    case GLFW_KEY_4:
        return Window::KeyboardKey::KEY_4;
    case GLFW_KEY_5:
        return Window::KeyboardKey::KEY_5;
    case GLFW_KEY_6:
        return Window::KeyboardKey::KEY_6;
    case GLFW_KEY_7:
        return Window::KeyboardKey::KEY_7;
    case GLFW_KEY_8:
        return Window::KeyboardKey::KEY_8;
    case GLFW_KEY_9:
        return Window::KeyboardKey::KEY_9;
    case GLFW_KEY_SEMICOLON:
        return Window::KeyboardKey::KEY_SEMICOLON;
    case GLFW_KEY_EQUAL:
        return Window::KeyboardKey::KEY_EQUAL;
    case GLFW_KEY_A:
        return Window::KeyboardKey::KEY_A;
    case GLFW_KEY_B:
        return Window::KeyboardKey::KEY_B;
    case GLFW_KEY_C:
        return Window::KeyboardKey::KEY_C;
    case GLFW_KEY_D:
        return Window::KeyboardKey::KEY_D;
    case GLFW_KEY_E:
        return Window::KeyboardKey::KEY_E;
    case GLFW_KEY_F:
        return Window::KeyboardKey::KEY_F;
    case GLFW_KEY_G:
        return Window::KeyboardKey::KEY_G;
    case GLFW_KEY_H:
        return Window::KeyboardKey::KEY_H;
    case GLFW_KEY_I:
        return Window::KeyboardKey::KEY_I;
    case GLFW_KEY_J:
        return Window::KeyboardKey::KEY_J;
    case GLFW_KEY_K:
        return Window::KeyboardKey::KEY_K;
    case GLFW_KEY_L:
        return Window::KeyboardKey::KEY_L;
    case GLFW_KEY_M:
        return Window::KeyboardKey::KEY_M;
    case GLFW_KEY_N:
        return Window::KeyboardKey::KEY_N;
    case GLFW_KEY_O:
        return Window::KeyboardKey::KEY_O;
    case GLFW_KEY_P:
        return Window::KeyboardKey::KEY_P;
    case GLFW_KEY_Q:
        return Window::KeyboardKey::KEY_Q;
    case GLFW_KEY_R:
        return Window::KeyboardKey::KEY_R;
    case GLFW_KEY_S:
        return Window::KeyboardKey::KEY_S;
    case GLFW_KEY_T:
        return Window::KeyboardKey::KEY_T;
    case GLFW_KEY_U:
        return Window::KeyboardKey::KEY_U;
    case GLFW_KEY_V:
        return Window::KeyboardKey::KEY_V;
    case GLFW_KEY_W:
        return Window::KeyboardKey::KEY_W;
    case GLFW_KEY_X:
        return Window::KeyboardKey::KEY_X;
    case GLFW_KEY_Y:
        return Window::KeyboardKey::KEY_Y;
    case GLFW_KEY_Z:
        return Window::KeyboardKey::KEY_Z;
    case GLFW_KEY_LEFT_BRACKET:
        return Window::KeyboardKey::KEY_LEFT_BRACKET;
    case GLFW_KEY_BACKSLASH:
        return Window::KeyboardKey::KEY_BACKSLASH;
    case GLFW_KEY_RIGHT_BRACKET:
        return Window::KeyboardKey::KEY_RIGHT_BRACKET;
    case GLFW_KEY_GRAVE_ACCENT:
        return Window::KeyboardKey::KEY_GRAVE_ACCENT;
    case GLFW_KEY_WORLD_1:
        return Window::KeyboardKey::KEY_WORLD_1;
    case GLFW_KEY_WORLD_2:
        return Window::KeyboardKey::KEY_WORLD_2;
    case GLFW_KEY_ESCAPE:
        return Window::KeyboardKey::KEY_ESCAPE;
    case GLFW_KEY_ENTER:
        return Window::KeyboardKey::KEY_ENTER;
    case GLFW_KEY_TAB:
        return Window::KeyboardKey::KEY_TAB;
    case GLFW_KEY_BACKSPACE:
        return Window::KeyboardKey::KEY_BACKSPACE;
    case GLFW_KEY_INSERT:
        return Window::KeyboardKey::KEY_INSERT;
    case GLFW_KEY_DELETE:
        return Window::KeyboardKey::KEY_DELETE;
    case GLFW_KEY_RIGHT:
        return Window::KeyboardKey::KEY_RIGHT;
    case GLFW_KEY_LEFT:
        return Window::KeyboardKey::KEY_LEFT;
    case GLFW_KEY_DOWN:
        return Window::KeyboardKey::KEY_DOWN;
    case GLFW_KEY_UP:
        return Window::KeyboardKey::KEY_UP;
    case GLFW_KEY_PAGE_UP:
        return Window::KeyboardKey::KEY_PAGE_UP;
    case GLFW_KEY_PAGE_DOWN:
        return Window::KeyboardKey::KEY_PAGE_DOWN;
    case GLFW_KEY_HOME:
        return Window::KeyboardKey::KEY_HOME;
    case GLFW_KEY_END:
        return Window::KeyboardKey::KEY_END;
    case GLFW_KEY_CAPS_LOCK:
        return Window::KeyboardKey::KEY_CAPS_LOCK;
    case GLFW_KEY_SCROLL_LOCK:
        return Window::KeyboardKey::KEY_SCROLL_LOCK;
    case GLFW_KEY_NUM_LOCK:
        return Window::KeyboardKey::KEY_NUM_LOCK;
    case GLFW_KEY_PRINT_SCREEN:
        return Window::KeyboardKey::KEY_PRINT_SCREEN;
    case GLFW_KEY_PAUSE:
        return Window::KeyboardKey::KEY_PAUSE;
    case GLFW_KEY_F1:
        return Window::KeyboardKey::KEY_F1;
    case GLFW_KEY_F2:
        return Window::KeyboardKey::KEY_F2;
    case GLFW_KEY_F3:
        return Window::KeyboardKey::KEY_F3;
    case GLFW_KEY_F4:
        return Window::KeyboardKey::KEY_F4;
    case GLFW_KEY_F5:
        return Window::KeyboardKey::KEY_F5;
    case GLFW_KEY_F6:
        return Window::KeyboardKey::KEY_F6;
    case GLFW_KEY_F7:
        return Window::KeyboardKey::KEY_F7;
    case GLFW_KEY_F8:
        return Window::KeyboardKey::KEY_F8;
    case GLFW_KEY_F9:
        return Window::KeyboardKey::KEY_F9;
    case GLFW_KEY_F10:
        return Window::KeyboardKey::KEY_F10;
    case GLFW_KEY_F11:
        return Window::KeyboardKey::KEY_F11;
    case GLFW_KEY_F12:
        return Window::KeyboardKey::KEY_F12;
    case GLFW_KEY_F13:
        return Window::KeyboardKey::KEY_F13;
    case GLFW_KEY_F14:
        return Window::KeyboardKey::KEY_F14;
    case GLFW_KEY_F15:
        return Window::KeyboardKey::KEY_F15;
    case GLFW_KEY_F16:
        return Window::KeyboardKey::KEY_F16;
    case GLFW_KEY_F17:
        return Window::KeyboardKey::KEY_F17;
    case GLFW_KEY_F18:
        return Window::KeyboardKey::KEY_F18;
    case GLFW_KEY_F19:
        return Window::KeyboardKey::KEY_F19;
    case GLFW_KEY_F20:
        return Window::KeyboardKey::KEY_F20;
    case GLFW_KEY_F21:
        return Window::KeyboardKey::KEY_F21;
    case GLFW_KEY_F22:
        return Window::KeyboardKey::KEY_F22;
    case GLFW_KEY_F23:
        return Window::KeyboardKey::KEY_F23;
    case GLFW_KEY_F24:
        return Window::KeyboardKey::KEY_F24;
    case GLFW_KEY_F25:
        return Window::KeyboardKey::KEY_F25;
    case GLFW_KEY_KP_0:
        return Window::KeyboardKey::KEY_KP_0;
    case GLFW_KEY_KP_1:
        return Window::KeyboardKey::KEY_KP_1;
    case GLFW_KEY_KP_2:
        return Window::KeyboardKey::KEY_KP_2;
    case GLFW_KEY_KP_3:
        return Window::KeyboardKey::KEY_KP_3;
    case GLFW_KEY_KP_4:
        return Window::KeyboardKey::KEY_KP_4;
    case GLFW_KEY_KP_5:
        return Window::KeyboardKey::KEY_KP_5;
    case GLFW_KEY_KP_6:
        return Window::KeyboardKey::KEY_KP_6;
    case GLFW_KEY_KP_7:
        return Window::KeyboardKey::KEY_KP_7;
    case GLFW_KEY_KP_8:
        return Window::KeyboardKey::KEY_KP_8;
    case GLFW_KEY_KP_9:
        return Window::KeyboardKey::KEY_KP_9;
    case GLFW_KEY_KP_DECIMAL:
        return Window::KeyboardKey::KEY_KP_DECIMAL;
    case GLFW_KEY_KP_DIVIDE:
        return Window::KeyboardKey::KEY_KP_DIVIDE;
    case GLFW_KEY_KP_MULTIPLY:
        return Window::KeyboardKey::KEY_KP_MULTIPLY;
    case GLFW_KEY_KP_SUBTRACT:
        return Window::KeyboardKey::KEY_KP_SUBTRACT;
    case GLFW_KEY_KP_ADD:
        return Window::KeyboardKey::KEY_KP_ADD;
    case GLFW_KEY_KP_ENTER:
        return Window::KeyboardKey::KEY_KP_ENTER;
    case GLFW_KEY_KP_EQUAL:
        return Window::KeyboardKey::KEY_KP_EQUAL;
    case GLFW_KEY_LEFT_SHIFT:
        return Window::KeyboardKey::KEY_LEFT_SHIFT;
    case GLFW_KEY_LEFT_CONTROL:
        return Window::KeyboardKey::KEY_LEFT_CONTROL;
    case GLFW_KEY_LEFT_ALT:
        return Window::KeyboardKey::KEY_LEFT_ALT;
    case GLFW_KEY_LEFT_SUPER:
        return Window::KeyboardKey::KEY_LEFT_SUPER;
    case GLFW_KEY_RIGHT_SHIFT:
        return Window::KeyboardKey::KEY_RIGHT_SHIFT;
    case GLFW_KEY_RIGHT_CONTROL:
        return Window::KeyboardKey::KEY_RIGHT_CONTROL;
    case GLFW_KEY_RIGHT_ALT:
        return Window::KeyboardKey::KEY_RIGHT_ALT;
    case GLFW_KEY_RIGHT_SUPER:
        return Window::KeyboardKey::KEY_RIGHT_SUPER;
    case GLFW_KEY_MENU:
        return Window::KeyboardKey::KEY_MENU;
    }
    return Window::KeyboardKey::KEY_UNKNOWN;
}

Window::KeyboardMods Window::GLFWCodeToKeyboardMods(
    int32_t pCode)
{
    int32_t lKeyboardMods = 0;
    if (pCode & GLFW_MOD_SHIFT)
    {
        lKeyboardMods |= static_cast<int32_t>(Window::KeyboardMod::MOD_SHIFT);
    }
    if (pCode & GLFW_MOD_CONTROL)
    {
        lKeyboardMods |= static_cast<int32_t>(Window::KeyboardMod::MOD_CONTROL);
    }
    if (pCode & GLFW_MOD_ALT)
    {
        lKeyboardMods |= static_cast<int32_t>(Window::KeyboardMod::MOD_ALT);
    }
    if (pCode & GLFW_MOD_SUPER)
    {
        lKeyboardMods |= static_cast<int32_t>(Window::KeyboardMod::MOD_SUPER);
    }
    if (pCode & GLFW_MOD_CAPS_LOCK)
    {
        lKeyboardMods |= static_cast<int32_t>(Window::KeyboardMod::MOD_CAPS_LOCK);
    }
    if (pCode & GLFW_MOD_NUM_LOCK)
    {
        lKeyboardMods |= static_cast<int32_t>(Window::KeyboardMod::MOD_NUM_LOCK);
    }
    return lKeyboardMods;
}

Window::MouseButton Window::GLFWCodeToMouseButton(
    int32_t pCode)
{
    switch (pCode)
    {
    case GLFW_MOUSE_BUTTON_1:
        return Window::MouseButton::BUTTON_1;
    case GLFW_MOUSE_BUTTON_2:
        return Window::MouseButton::BUTTON_2;
    case GLFW_MOUSE_BUTTON_3:
        return Window::MouseButton::BUTTON_3;
    case GLFW_MOUSE_BUTTON_4:
        return Window::MouseButton::BUTTON_4;
    case GLFW_MOUSE_BUTTON_5:
        return Window::MouseButton::BUTTON_5;
    case GLFW_MOUSE_BUTTON_6:
        return Window::MouseButton::BUTTON_6;
    case GLFW_MOUSE_BUTTON_7:
        return Window::MouseButton::BUTTON_7;
    case GLFW_MOUSE_BUTTON_8:
        return Window::MouseButton::BUTTON_8;
    }
    return Window::MouseButton::BUTTON_1;
}

int32_t Window::KeyboardKeyToGLFWCode(
    Window::KeyboardKey pCode)
{
    switch (pCode)
    {
    case Window::KeyboardKey::KEY_UNKNOWN:
        return GLFW_KEY_UNKNOWN;
    case Window::KeyboardKey::KEY_SPACE:
        return GLFW_KEY_SPACE;
    case Window::KeyboardKey::KEY_APOSTROPHE:
        return GLFW_KEY_APOSTROPHE;
    case Window::KeyboardKey::KEY_COMMA:
        return GLFW_KEY_COMMA;
    case Window::KeyboardKey::KEY_MINUS:
        return GLFW_KEY_MINUS;
    case Window::KeyboardKey::KEY_PERIOD:
        return GLFW_KEY_PERIOD;
    case Window::KeyboardKey::KEY_SLASH:
        return GLFW_KEY_SLASH;
    case Window::KeyboardKey::KEY_0:
        return GLFW_KEY_0;
    case Window::KeyboardKey::KEY_1:
        return GLFW_KEY_1;
    case Window::KeyboardKey::KEY_2:
        return GLFW_KEY_2;
    case Window::KeyboardKey::KEY_3:
        return GLFW_KEY_3;
    case Window::KeyboardKey::KEY_4:
        return GLFW_KEY_4;
    case Window::KeyboardKey::KEY_5:
        return GLFW_KEY_5;
    case Window::KeyboardKey::KEY_6:
        return GLFW_KEY_6;
    case Window::KeyboardKey::KEY_7:
        return GLFW_KEY_7;
    case Window::KeyboardKey::KEY_8:
        return GLFW_KEY_8;
    case Window::KeyboardKey::KEY_9:
        return GLFW_KEY_9;
    case Window::KeyboardKey::KEY_SEMICOLON:
        return GLFW_KEY_SEMICOLON;
    case Window::KeyboardKey::KEY_EQUAL:
        return GLFW_KEY_EQUAL;
    case Window::KeyboardKey::KEY_A:
        return GLFW_KEY_A;
    case Window::KeyboardKey::KEY_B:
        return GLFW_KEY_B;
    case Window::KeyboardKey::KEY_C:
        return GLFW_KEY_C;
    case Window::KeyboardKey::KEY_D:
        return GLFW_KEY_D;
    case Window::KeyboardKey::KEY_E:
        return GLFW_KEY_E;
    case Window::KeyboardKey::KEY_F:
        return GLFW_KEY_F;
    case Window::KeyboardKey::KEY_G:
        return GLFW_KEY_G;
    case Window::KeyboardKey::KEY_H:
        return GLFW_KEY_H;
    case Window::KeyboardKey::KEY_I:
        return GLFW_KEY_I;
    case Window::KeyboardKey::KEY_J:
        return GLFW_KEY_J;
    case Window::KeyboardKey::KEY_K:
        return GLFW_KEY_K;
    case Window::KeyboardKey::KEY_L:
        return GLFW_KEY_L;
    case Window::KeyboardKey::KEY_M:
        return GLFW_KEY_M;
    case Window::KeyboardKey::KEY_N:
        return GLFW_KEY_N;
    case Window::KeyboardKey::KEY_O:
        return GLFW_KEY_O;
    case Window::KeyboardKey::KEY_P:
        return GLFW_KEY_P;
    case Window::KeyboardKey::KEY_Q:
        return GLFW_KEY_Q;
    case Window::KeyboardKey::KEY_R:
        return GLFW_KEY_R;
    case Window::KeyboardKey::KEY_S:
        return GLFW_KEY_S;
    case Window::KeyboardKey::KEY_T:
        return GLFW_KEY_T;
    case Window::KeyboardKey::KEY_U:
        return GLFW_KEY_U;
    case Window::KeyboardKey::KEY_V:
        return GLFW_KEY_V;
    case Window::KeyboardKey::KEY_W:
        return GLFW_KEY_W;
    case Window::KeyboardKey::KEY_X:
        return GLFW_KEY_X;
    case Window::KeyboardKey::KEY_Y:
        return GLFW_KEY_Y;
    case Window::KeyboardKey::KEY_Z:
        return GLFW_KEY_Z;
    case Window::KeyboardKey::KEY_LEFT_BRACKET:
        return GLFW_KEY_LEFT_BRACKET;
    case Window::KeyboardKey::KEY_BACKSLASH:
        return GLFW_KEY_BACKSLASH;
    case Window::KeyboardKey::KEY_RIGHT_BRACKET:
        return GLFW_KEY_RIGHT_BRACKET;
    case Window::KeyboardKey::KEY_GRAVE_ACCENT:
        return GLFW_KEY_GRAVE_ACCENT;
    case Window::KeyboardKey::KEY_WORLD_1:
        return GLFW_KEY_WORLD_1;
    case Window::KeyboardKey::KEY_WORLD_2:
        return GLFW_KEY_WORLD_2;
    case Window::KeyboardKey::KEY_ESCAPE:
        return GLFW_KEY_ESCAPE;
    case Window::KeyboardKey::KEY_ENTER:
        return GLFW_KEY_ENTER;
    case Window::KeyboardKey::KEY_TAB:
        return GLFW_KEY_TAB;
    case Window::KeyboardKey::KEY_BACKSPACE:
        return GLFW_KEY_BACKSPACE;
    case Window::KeyboardKey::KEY_INSERT:
        return GLFW_KEY_INSERT;
    case Window::KeyboardKey::KEY_DELETE:
        return GLFW_KEY_DELETE;
    case Window::KeyboardKey::KEY_RIGHT:
        return GLFW_KEY_RIGHT;
    case Window::KeyboardKey::KEY_LEFT:
        return GLFW_KEY_LEFT;
    case Window::KeyboardKey::KEY_DOWN:
        return GLFW_KEY_DOWN;
    case Window::KeyboardKey::KEY_UP:
        return GLFW_KEY_UP;
    case Window::KeyboardKey::KEY_PAGE_UP:
        return GLFW_KEY_PAGE_UP;
    case Window::KeyboardKey::KEY_PAGE_DOWN:
        return GLFW_KEY_PAGE_DOWN;
    case Window::KeyboardKey::KEY_HOME:
        return GLFW_KEY_HOME;
    case Window::KeyboardKey::KEY_END:
        return GLFW_KEY_END;
    case Window::KeyboardKey::KEY_CAPS_LOCK:
        return GLFW_KEY_CAPS_LOCK;
    case Window::KeyboardKey::KEY_SCROLL_LOCK:
        return GLFW_KEY_SCROLL_LOCK;
    case Window::KeyboardKey::KEY_NUM_LOCK:
        return GLFW_KEY_NUM_LOCK;
    case Window::KeyboardKey::KEY_PRINT_SCREEN:
        return GLFW_KEY_PRINT_SCREEN;
    case Window::KeyboardKey::KEY_PAUSE:
        return GLFW_KEY_PAUSE;
    case Window::KeyboardKey::KEY_F1:
        return GLFW_KEY_F1;
    case Window::KeyboardKey::KEY_F2:
        return GLFW_KEY_F2;
    case Window::KeyboardKey::KEY_F3:
        return GLFW_KEY_F3;
    case Window::KeyboardKey::KEY_F4:
        return GLFW_KEY_F4;
    case Window::KeyboardKey::KEY_F5:
        return GLFW_KEY_F5;
    case Window::KeyboardKey::KEY_F6:
        return GLFW_KEY_F6;
    case Window::KeyboardKey::KEY_F7:
        return GLFW_KEY_F7;
    case Window::KeyboardKey::KEY_F8:
        return GLFW_KEY_F8;
    case Window::KeyboardKey::KEY_F9:
        return GLFW_KEY_F9;
    case Window::KeyboardKey::KEY_F10:
        return GLFW_KEY_F10;
    case Window::KeyboardKey::KEY_F11:
        return GLFW_KEY_F11;
    case Window::KeyboardKey::KEY_F12:
        return GLFW_KEY_F12;
    case Window::KeyboardKey::KEY_F13:
        return GLFW_KEY_F13;
    case Window::KeyboardKey::KEY_F14:
        return GLFW_KEY_F14;
    case Window::KeyboardKey::KEY_F15:
        return GLFW_KEY_F15;
    case Window::KeyboardKey::KEY_F16:
        return GLFW_KEY_F16;
    case Window::KeyboardKey::KEY_F17:
        return GLFW_KEY_F17;
    case Window::KeyboardKey::KEY_F18:
        return GLFW_KEY_F18;
    case Window::KeyboardKey::KEY_F19:
        return GLFW_KEY_F19;
    case Window::KeyboardKey::KEY_F20:
        return GLFW_KEY_F20;
    case Window::KeyboardKey::KEY_F21:
        return GLFW_KEY_F21;
    case Window::KeyboardKey::KEY_F22:
        return GLFW_KEY_F22;
    case Window::KeyboardKey::KEY_F23:
        return GLFW_KEY_F23;
    case Window::KeyboardKey::KEY_F24:
        return GLFW_KEY_F24;
    case Window::KeyboardKey::KEY_F25:
        return GLFW_KEY_F25;
    case Window::KeyboardKey::KEY_KP_0:
        return GLFW_KEY_KP_0;
    case Window::KeyboardKey::KEY_KP_1:
        return GLFW_KEY_KP_1;
    case Window::KeyboardKey::KEY_KP_2:
        return GLFW_KEY_KP_2;
    case Window::KeyboardKey::KEY_KP_3:
        return GLFW_KEY_KP_3;
    case Window::KeyboardKey::KEY_KP_4:
        return GLFW_KEY_KP_4;
    case Window::KeyboardKey::KEY_KP_5:
        return GLFW_KEY_KP_5;
    case Window::KeyboardKey::KEY_KP_6:
        return GLFW_KEY_KP_6;
    case Window::KeyboardKey::KEY_KP_7:
        return GLFW_KEY_KP_7;
    case Window::KeyboardKey::KEY_KP_8:
        return GLFW_KEY_KP_8;
    case Window::KeyboardKey::KEY_KP_9:
        return GLFW_KEY_KP_9;
    case Window::KeyboardKey::KEY_KP_DECIMAL:
        return GLFW_KEY_KP_DECIMAL;
    case Window::KeyboardKey::KEY_KP_DIVIDE:
        return GLFW_KEY_KP_DIVIDE;
    case Window::KeyboardKey::KEY_KP_MULTIPLY:
        return GLFW_KEY_KP_MULTIPLY;
    case Window::KeyboardKey::KEY_KP_SUBTRACT:
        return GLFW_KEY_KP_SUBTRACT;
    case Window::KeyboardKey::KEY_KP_ADD:
        return GLFW_KEY_KP_ADD;
    case Window::KeyboardKey::KEY_KP_ENTER:
        return GLFW_KEY_KP_ENTER;
    case Window::KeyboardKey::KEY_KP_EQUAL:
        return GLFW_KEY_KP_EQUAL;
    case Window::KeyboardKey::KEY_LEFT_SHIFT:
        return GLFW_KEY_LEFT_SHIFT;
    case Window::KeyboardKey::KEY_LEFT_CONTROL:
        return GLFW_KEY_LEFT_CONTROL;
    case Window::KeyboardKey::KEY_LEFT_ALT:
        return GLFW_KEY_LEFT_ALT;
    case Window::KeyboardKey::KEY_LEFT_SUPER:
        return GLFW_KEY_LEFT_SUPER;
    case Window::KeyboardKey::KEY_RIGHT_SHIFT:
        return GLFW_KEY_RIGHT_SHIFT;
    case Window::KeyboardKey::KEY_RIGHT_CONTROL:
        return GLFW_KEY_RIGHT_CONTROL;
    case Window::KeyboardKey::KEY_RIGHT_ALT:
        return GLFW_KEY_RIGHT_ALT;
    case Window::KeyboardKey::KEY_RIGHT_SUPER:
        return GLFW_KEY_RIGHT_SUPER;
    case Window::KeyboardKey::KEY_MENU:
        return GLFW_KEY_MENU;
    }
    return GLFW_KEY_UNKNOWN;
}

int32_t Window::MouseButtonToGLFWCode(
    Window::MouseButton pCode)
{
    switch (pCode)
    {
    case Window::MouseButton::BUTTON_1:
        return GLFW_MOUSE_BUTTON_1;
    case Window::MouseButton::BUTTON_2:
        return GLFW_MOUSE_BUTTON_2;
    case Window::MouseButton::BUTTON_3:
        return GLFW_MOUSE_BUTTON_3;
    case Window::MouseButton::BUTTON_4:
        return GLFW_MOUSE_BUTTON_4;
    case Window::MouseButton::BUTTON_5:
        return GLFW_MOUSE_BUTTON_5;
    case Window::MouseButton::BUTTON_6:
        return GLFW_MOUSE_BUTTON_6;
    case Window::MouseButton::BUTTON_7:
        return GLFW_MOUSE_BUTTON_7;
    case Window::MouseButton::BUTTON_8:
        return GLFW_MOUSE_BUTTON_8;
    }
    return GLFW_MOUSE_BUTTON_1;
}
