#ifndef SHADER_HPP_INCLUDED
#define SHADER_HPP_INCLUDED

#include <glm/glm.hpp>

#include <cstdint>
#include <memory>
#include <string>

class Shader
{
public:
    Shader(
        const std::string& pFilename);

    virtual ~Shader();

    Shader(const Shader&) = delete;
    Shader& operator=(const Shader&) = delete;

    Shader(Shader&&) = delete;
    Shader& operator=(Shader&&) = delete;

    bool IsNull() const;

    void Use() const;

    void SetColor(
        const glm::vec4& pColor);

    void SetTexture(
        uint32_t pTextureUnit);

    void SetTransform(
        const glm::mat4& pTransform);

    void SetProjection(
        const glm::mat4& pProjection);

protected:
    uint32_t mHandle;
};

#endif // SHADER_HPP_INCLUDED
