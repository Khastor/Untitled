#include <Texture.hpp>

#include <stb/stb_image.h>

#include <glad/gl.h>

Texture::Texture(
    const std::string& pFilename) :
    mHandle(0)
{
    int lWidth, lHeight, lChannels;
    if (uint8_t* lTexels = stbi_load(pFilename.c_str(), &lWidth, &lHeight, &lChannels, 0); lChannels == 4)
    {
        glCreateTextures(GL_TEXTURE_2D, 1, &mHandle);
        glTextureStorage2D(mHandle, 1, GL_RGBA8, lWidth, lHeight);
        glTextureSubImage2D(mHandle, 0, 0, 0, lWidth, lHeight, GL_RGBA, GL_UNSIGNED_BYTE, lTexels);

        glTextureParameteri(mHandle, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTextureParameteri(mHandle, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTextureParameteri(mHandle, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTextureParameteri(mHandle, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        stbi_image_free(lTexels);
    }
}

Texture::~Texture()
{
    glDeleteTextures(1, &mHandle);
}

std::shared_ptr<Texture> Texture::Create(
    const std::string& pFilename)
{
    return std::make_shared<Texture>(pFilename);
}

bool Texture::IsNull() const
{
    return mHandle == 0;
}

void Texture::Bind(
    uint32_t pUnit) const
{
    glBindTextureUnit(pUnit, mHandle);
}
