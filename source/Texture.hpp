#ifndef TEXTURE_HPP_INCLUDED
#define TEXTURE_HPP_INCLUDED

#include <cstdint>
#include <memory>
#include <string>

class Texture
{
public:
    Texture(
        const std::string& pFilename);

    ~Texture();

    Texture(const Texture&) = delete;
    Texture& operator=(const Texture&) = delete;

    Texture(Texture&&) = delete;
    Texture& operator=(Texture&&) = delete;

    static std::shared_ptr<Texture> Create(
        const std::string& pFilename);

    bool IsNull() const;

    void Bind(
        uint32_t pUnit) const;

private:
    uint32_t mHandle;
};

#endif // TEXTURE_HPP_INCLUDED
