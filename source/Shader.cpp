#include <Shader.hpp>

#include <glad/gl.h>

#include <glm/gtc/type_ptr.hpp>

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

namespace
{

std::string ReadShaderSource(
    const std::string& pFilename)
{
    if (std::ifstream lFile = std::ifstream(pFilename))
    {
        return (std::stringstream() << lFile.rdbuf()).str();
    }
    return {};
}

bool CheckShaderCompileStatus(
    uint32_t pShaderHandle)
{
    GLint lCompileStatus;
    glGetShaderiv(pShaderHandle, GL_COMPILE_STATUS, &lCompileStatus);

    GLint lInfoLogLength;
    glGetShaderiv(pShaderHandle, GL_INFO_LOG_LENGTH, &lInfoLogLength);

    if (lInfoLogLength)
    {
        std::string lInfoLog(lInfoLogLength, 0);
        glGetShaderInfoLog(pShaderHandle, lInfoLogLength, nullptr, lInfoLog.data());

        std::cerr << lInfoLog << std::endl;
    }

    return lCompileStatus;
}

bool CheckProgramLinkStatus(
    uint32_t pProgramHandle)
{
    GLint lLinkStatus;
    glGetProgramiv(pProgramHandle, GL_LINK_STATUS, &lLinkStatus);

    GLint lInfoLogLength;
    glGetProgramiv(pProgramHandle, GL_INFO_LOG_LENGTH, &lInfoLogLength);

    if (lInfoLogLength)
    {
        std::string lInfoLog(lInfoLogLength, 0);
        glGetProgramInfoLog(pProgramHandle, lInfoLogLength, nullptr, lInfoLog.data());

        std::cerr << lInfoLog << std::endl;
    }

    return lLinkStatus;
}

}

Shader::Shader(
    const std::string& pFilename) :
    mHandle(0)
{
    std::string lVertexShaderSource = ReadShaderSource(pFilename + ".vert");
    std::string lFragmentShaderSource = ReadShaderSource(pFilename + ".frag");

    GLuint lVertexShaderHandle = glCreateShader(GL_VERTEX_SHADER);
    char* lVertexShaderSourceData = lVertexShaderSource.data();
    glShaderSource(lVertexShaderHandle, 1, &lVertexShaderSourceData, 0);
    glCompileShader(lVertexShaderHandle);
    if (!CheckShaderCompileStatus(lVertexShaderHandle))
    {
        glDeleteShader(lVertexShaderHandle);
        return;
    }

    GLuint lFragmentShaderHandle = glCreateShader(GL_FRAGMENT_SHADER);
    char* lFragmentShaderSourceData = lFragmentShaderSource.data();
    glShaderSource(lFragmentShaderHandle, 1, &lFragmentShaderSourceData, 0);
    glCompileShader(lFragmentShaderHandle);
    if (!CheckShaderCompileStatus(lFragmentShaderHandle))
    {
        glDeleteShader(lVertexShaderHandle);
        glDeleteShader(lFragmentShaderHandle);
        return;
    }

    mHandle = glCreateProgram();
    glAttachShader(mHandle, lVertexShaderHandle);
    glAttachShader(mHandle, lFragmentShaderHandle);
    glLinkProgram(mHandle);
    if (!CheckProgramLinkStatus(mHandle))
    {
        glDeleteShader(lVertexShaderHandle);
        glDeleteShader(lFragmentShaderHandle);
        glDeleteProgram(mHandle);
        mHandle = 0;
        return;
    }

    glDeleteShader(lVertexShaderHandle);
    glDeleteShader(lFragmentShaderHandle);
}

Shader::~Shader()
{
    glDeleteProgram(mHandle);
}

bool Shader::IsNull() const
{
    return mHandle == 0;
}

void Shader::Use() const
{
    glUseProgram(mHandle);
}

void Shader::SetColor(
    const glm::vec4& pColor)
{
    GLint lColorLocation = glGetUniformLocation(mHandle, "uColor");
    glUniform4fv(lColorLocation, 1, glm::value_ptr(pColor));
}

void Shader::SetTexture(
    uint32_t pTextureUnit)
{
    GLint lTextureLocation = glGetUniformLocation(mHandle, "uTexture");
    glUniform1i(lTextureLocation, pTextureUnit);
}

void Shader::SetTransform(
    const glm::mat4& pTransform)
{
    GLint lTransformLocation = glGetUniformLocation(mHandle, "uTransform");
    glUniformMatrix4fv(lTransformLocation, 1, GL_FALSE, glm::value_ptr(pTransform));
}

void Shader::SetProjection(
    const glm::mat4& pProjection)
{
    GLint lProjectionLocation = glGetUniformLocation(mHandle, "uProjection");
    glUniformMatrix4fv(lProjectionLocation, 1, GL_FALSE, glm::value_ptr(pProjection));
}
