#include <InputState.hpp>
#include <Shader.hpp>
#include <TextureArray.hpp>
#include <Window.hpp>
#include <Core/Camera.hpp>
#include <Core/CharacterVertexArray.hpp>
#include <Core/Direction.hpp>
#include <Core/Numeric.hpp>
#include <Core/TerrainFactory.hpp>
#include <Core/TerrainObject.hpp>
#include <Core/Behaviours/CharacterMoveBehaviour.hpp>
#include <Debug/Overlay.hpp>

#include <glad/gl.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <cmath>
#include <numeric>
#include <vector>
#include <cstdint>
#include <random>
#include <limits>

class UntitledWindow : public Window
{
public:
    UntitledWindow();

protected:
    bool OnCreate() override;

    bool OnUpdate(
        double pCurrentTime,
        double pElapsedTime) override;

    void OnDestroy() override;

    void OnKeyPressed(
        Window::KeyboardKey pKey,
        Window::KeyboardMods pMods) override;

    void OnKeyReleased(
        Window::KeyboardKey pKey,
        Window::KeyboardMods pMods) override;

    void OnMouseScrolled(
        double pOffsetX,
        double pOffsetY) override;

    Core::TerrainFactory mTerrainFactory;
    std::shared_ptr<TextureArray> mTexture;
    std::shared_ptr<Shader> mShader;
    std::shared_ptr<Core::TerrainObject> mTerrain;
    std::shared_ptr<Shader> mCharacterShader;
    std::shared_ptr<Core::CharacterVertexArray> mCharacterVertexArray;

    Core::WorldCoord mCharacterPosition;
    Core::CharacterMoveCommands mCharacterCommands;

    Core::Camera mCamera;
    InputState mInputState;

    std::shared_ptr<Debug::Overlay> mDebugOverlay;
};

UntitledWindow::UntitledWindow() :
    Window(4, 6, 1, "Untitled")
{}

bool UntitledWindow::OnCreate()
{
    mDebugOverlay = std::make_shared<Debug::Overlay>();

    mShader = std::make_shared<Shader>("assets/shaders/Shader");
    mTexture = std::make_shared<TextureArray>("assets/tilesets/textures/rocks-array.png", 128, 128, 12);

    mCharacterShader = std::make_shared<Shader>("assets/shaders/Character");
    mCharacterVertexArray = std::make_shared<Core::CharacterVertexArray>(1.0f, 0.0f, 0.0f);
    mCharacterPosition = Core::WorldCoord{ 2, 2 }.Centered();

    // make maze view
    mTerrain = std::make_shared<Core::TerrainObject>(mTerrainFactory, 50);

    //Core::WorldCoord lInitialCameraPosition{ std::numeric_limits<uint32_t>::max() - 2, std::numeric_limits<uint32_t>::max() - 2 };
    Core::WorldCoord lInitialCameraPosition{ 2, 2 };
    mCamera.SetZoom(15.0f);
    mCamera.SetPosition(lInitialCameraPosition);
    mTerrain->CenterOn(lInitialCameraPosition);

    SetFramerateFixed(true);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    return true;
}

bool UntitledWindow::OnUpdate(
    double pCurrentTime,
    double pElapsedTime)
{
    mCamera.SetAspectRatio(static_cast<float>(GetFramebufferWidth()) / GetFramebufferHeight());
    mCamera.SetTargetPosition(mCharacterPosition);
    const Core::LocalView lCameraView(mCamera.GetLocalView());

    const float lCharacterSpeed = 5.0f;

    Core::CharacterMoveBehaviour lMoveBehaviour;
    lMoveBehaviour.SetMaxPathLength(100);
    lMoveBehaviour.Update(lCameraView, mCharacterPosition, mCharacterCommands, mTerrainFactory, lCharacterSpeed * pElapsedTime);

    // TODO: always check that an object is within local view bounds.
    Core::LocalCoordF lCameraLocalCoord = lCameraView.MapToLocal(mCamera.GetPosition());
    Core::LocalFrameF lCameraLocalFrame = lCameraView.MapToLocal(mCamera.GetFrame());

    // Update world terrain.
    mTerrain->CenterOn(mCamera.GetPosition());

    // Begin rendering.
    glClear(GL_COLOR_BUFFER_BIT);

    // Prepare the terrain for rendering.
    mTexture->Bind(0);
    mShader->Use();
    mShader->SetTexture(0);
    mShader->SetProjection(glm::ortho(lCameraLocalFrame.Min.X, lCameraLocalFrame.Max.X, lCameraLocalFrame.Min.Y, lCameraLocalFrame.Max.Y));

    Core::LocalCoordF lTerrainLocalCoord = lCameraView.MapToLocal(mTerrain->GetPosition());
    mShader->SetTransform(glm::translate(glm::identity<glm::mat4>(), glm::vec3(lTerrainLocalCoord.X + 0.5f, lTerrainLocalCoord.Y + 0.5f, 0.0f)));

    mTerrain->Draw();

    mCharacterShader->Use();
    mCharacterShader->SetProjection(glm::ortho(lCameraLocalFrame.Min.X, lCameraLocalFrame.Max.X, lCameraLocalFrame.Min.Y, lCameraLocalFrame.Max.Y));
    Core::LocalCoordF lCharacterLocalCoord = lCameraView.MapToLocal(mCharacterPosition);
    mCharacterShader->SetTransform(glm::translate(glm::identity<glm::mat4>(), glm::vec3(lCharacterLocalCoord.X, lCharacterLocalCoord.Y, 0.0f)));
    mCharacterVertexArray->Draw();

    mDebugOverlay->Clear();
    mDebugOverlay->Add(mCamera.GetPosition(), glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));
    for (auto& lPoint : lMoveBehaviour.mPath)
    {
        mDebugOverlay->Add(lPoint.Center, glm::vec4(1.0f, 0.0f, 1.0f, 1.0f));
    }
    mDebugOverlay->Draw(mCamera);
    return true;
}

void UntitledWindow::OnDestroy()
{}

void UntitledWindow::OnKeyPressed(
    Window::KeyboardKey pKey,
    Window::KeyboardMods pMods)
{
    switch (pKey)
    {
    case Window::KeyboardKey::KEY_F11:
        SetFullScreen(!IsFullScreen());
        break;
    case Window::KeyboardKey::KEY_W:
        mInputState.MoveUp = true;
        mCharacterCommands.Insert(Core::Direction::North);
        break;
    case Window::KeyboardKey::KEY_A:
        mInputState.MoveLeft = true;
        mCharacterCommands.Insert(Core::Direction::West);
        break;
    case Window::KeyboardKey::KEY_S:
        mInputState.MoveDown = true;
        mCharacterCommands.Insert(Core::Direction::South);
        break;
    case Window::KeyboardKey::KEY_D:
        mInputState.MoveRight= true;
        mCharacterCommands.Insert(Core::Direction::East);
        break;
    default:
        break;
    }
}

void UntitledWindow::OnKeyReleased(
    Window::KeyboardKey pKey,
    Window::KeyboardMods pMods)
{
    switch (pKey)
    {
    case Window::KeyboardKey::KEY_W:
        mInputState.MoveUp = false;
        break;
    case Window::KeyboardKey::KEY_A:
        mInputState.MoveLeft = false;
        break;
    case Window::KeyboardKey::KEY_S:
        mInputState.MoveDown= false;
        break;
    case Window::KeyboardKey::KEY_D:
        mInputState.MoveRight = false;
        break;
    }
}

void UntitledWindow::OnMouseScrolled(
    double pOffsetX,
    double pOffsetY)
{
    mCamera.SetZoom(mCamera.GetZoom() + pOffsetY);
}

int main()
{
    UntitledWindow().Show(800, 800);
    return 0;
}
