#version 460 core

in vec3 gTextureCoord;
out vec4 gFragmentColor;

uniform sampler2DArray uTexture;

void main()
{
    gFragmentColor = texture(uTexture, gTextureCoord.xyz).rgba;
}
