#version 460 core

in vec4 gVertexColor;
out vec4 gFragmentColor;

void main()
{
    gFragmentColor = gVertexColor;
}
