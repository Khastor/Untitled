#version 460 core

in vec3 gVertexColor;
out vec4 gFragmentColor;

void main()
{
    gFragmentColor = vec4(gVertexColor, 1.0);
}
