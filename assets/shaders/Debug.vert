#version 460 core

layout (location = 0) in vec2 aVertexCoord;
layout (location = 1) in vec4 aVertexColor;

out vec4 gVertexColor;

uniform mat4 uProjection;
uniform mat4 uTransform;

void main()
{
    gl_Position = uProjection * uTransform * vec4(aVertexCoord.xy, 0.0, 1.0);
    gVertexColor = aVertexColor;
}
